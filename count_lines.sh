echo "Counting Config files lines: "

config_total=0
for file in app/Config/App.php app/Config/Filters.php app/Config/Logger.php app/Config/Paths.php app/Config/Routes.php; do
    count=$(wc -l $file | cut -d " " -f 1)
    config_total=`expr $config_total + $count`
    printf "\t$file => $count lines\n"
done
printf "Total LOCs in Config => $config_total\n\n\n"

echo "Counting Model file lines: "
model_total=0
for file in app/Models/*; do
    count=$(wc -l $file | cut -d " " -f 1)
    model_total=`expr $model_total + $count`
    printf "\t$file => $count lines\n"
done
printf "Total LOCs in Models => $model_total\n\n\n"

echo "Counting Views file lines: "
view_total=0
for folder in app/Views/*; do
    if [[ -d $folder ]]; then
        for file in `ls $folder`; do
            [ -d "$folder/$file" ] && continue
            count=$(wc -l $folder/$file | cut -d " " -f 1)
            view_total=`expr $view_total + $count`
            printf "\t$folder/$file => $count lines\n"
        done
    else
        count=$(wc -l $folder | cut -d " " -f 1)
        view_total=`expr $view_total + $count`
        printf "\t$folder => $count lines\n"
    fi
done
printf "Total LOCs in Views => $view_total\n\n\n"

echo "Counting Controllers file lines: "
controller_total=0
for file in app/Controllers/*; do
    count=$(wc -l $file | cut -d " " -f 1)
    controller_total=`expr $controller_total + $count`
    printf "\t$file => $count\n"
done
printf "Total LOCs in Controllers => $controller_total\n\n\n"

echo "Counting Static file lines: "
static_total=0
for folder in public/static/css/*; do
    if [[ -d $folder ]]; then
        for file in `ls $folder`; do
            count=$(wc -l $folder/$file | cut -d " " -f 1)
            static_total=`expr $static_total + $count`
            printf "\t$folder/$file => $count lines\n"
        done
    else
        count=$(wc -l $folder | cut -d " " -f 1)
        static_total=`expr $static_total + $count`
        printf "\t$folder => $count lines\n"
    fi
done
for file in public/static/js/*; do
    count=$(wc -l $file | cut -d " " -f 1)
    static_total=`expr $static_total + $count`
    printf "\t$file => $count lines\n"
done
printf "Total LOCs in Static => $static_total\n\n\n"

echo "Counting Libraries file lines: "
library_total=$(wc -l app/Libraries/PatternRules.php | cut -d " " -f 1)
printf "\tapp/Libraries/PatternRules.php => $library_total\n"
printf "Total LOCs in Libraries => $library_total\n\n\n"

total_lines=`expr $static_total + $controller_total + $view_total + $model_total + $config_total + $library_total`
echo "Total LOCs => $total_lines lines of code"
