<?php

    /**
     * A helper to create URLs from the defined routes. Unlike framework methods,
     * This method allows for sending GET parameters using an array.
     * @param string $route => Name of the route to create the URL for
     * @param array  $subparts => URL subparts
     * @param array  $params => Array of all the GET parameters that can be sent to the URL
     *
     * @return false|string
    **/
    function url_for(string $route, array $subparts, array $params): ?string {
    $url = route_to($route, ...$subparts);

    if (is_null($params) === false) {
        foreach ($params as $param)
            $url .= "&" . $param . "=" . $params[ $param ];
    }

    return $url;
}