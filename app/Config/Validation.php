<?php
namespace Config;

class Validation {
    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var array
     */
    public $ruleSets = [
            \CodeIgniter\Validation\Rules::class,
            \CodeIgniter\Validation\FormatRules::class,
            \CodeIgniter\Validation\FileRules::class,
            \CodeIgniter\Validation\CreditCardRules::class,
            \App\Libraries\PatternRules::class
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array
     */
    public $templates = [
            'list'   => 'CodeIgniter\Validation\Views\list',
            'single' => 'CodeIgniter\Validation\Views\single',
    ];

    //--------------------------------------------------------------------
    // Rules
    //--------------------------------------------------------------------

    //------------------------- Password ---------------------------------
    public $password = [
        "password" => "required|password_pattern|min_length[10]",
        "cpassword" => "required_with[password]|matches[password]",
    ];
    public $password_errors = [
        "password" => [
            "min_length" => "Password must be of minimum 10 characters long.",
            "password_pattern" => "Password must contain atleast <ul><li> 1 alphabet </li><li> 1 number </li><li> 1 of ~,!,#,$,%,&,`,-,_,+,=,|,:,. </li></ul>"
        ],

        "cpassword" => [
            "matches" => "Both the passwords must be same"
        ]
    ];

    public $student = [
        "id" => "required|alpha_numeric|is_unique[student.id]",
        "password" => "permit_empty|min_length[10]|password_pattern",
        "name" => "required|alpha_space",
        "picture" => "permit_empty|is_image",
        "batch" => "required|alpha_numeric"
    ];

    public $teacher = [
        "id" => "required|alpha_numeric|is_unique[teacher.id]",
        "password" => "permit_empty|min_length[10]|password_pattern",
        "name" => "required|alpha_space",
        "picture" => "permit_empty|is_image",
    ];

    public $notice = [
        "title" => "required|alpha_numeric_punct",
        "text" => "permit_empty|alpha_numeric_punct",
        "batch" => "required|alpha_numeric"
    ];

    public $notice_errors = [
        "title" => [
            "required" => "Missing required title",
            "alpha_numeric_punct" => "Only alphabets, numbers, spaces and punctuations are allowed in the title"
        ],
        
        "text" => [
            "alpha_numeric_space" => "Only alphabets, numbers, spaces and punctuations are allowed in the body"
        ],
        
        "batch" =>[
            "required" => "No batch mentioned",
            "alpha_numeric" => "Invalid batch ID"
        ]
    ];

    public $course = [
        "id" => "required|alpha_numeric|min_length[2]|max_length[15]|is_unique[course.id]",
        "name" => "required|course_pattern|max_length[100]|is_unique_sanitized[course.name]"
    ];

    public $course_errors =[
        "id" => [
            "required" => "No course code provided",
            "alpha_numeric" => "Improper format for course code",
            "max_length" => "Course code too long",
            "min_length" => "Course code must minimum have 2 characters"
        ],
        "name" => [
            "required" => "No course name provided",
            "alpha_space" => "Illegal characters for course name",
            "is_unique_sanitized" => "Course with same already exists"
        ]
    ];

    public $batch = [
        "id" => "required|alpha_numeric|max_length[100]|is_unique[batch.id]",
        "course" => "required|is_not_unique[course.id]"
    ];

    public $batch_errors = [
        "id" => [
            "required" => "batch ID required",
            "alpha_numeric" => "Improper format for course code",
            "max_length" => "Course code too long",
            "is_unique" => "Batch ID already exists. Cannot add duplicate"
        ],
        "course" => [
            "required" => "No course selected",
            "is_not_unique" => "Course not present in database"
        ]
    ];

    public $subject = [
        "subjectCode" => "required|alpha_numeric|is_unique[subject.code]",
        "subjectName" => "required|alpha_space|max_length[500]",
        "course" => "required|alpha_numeric|min_length[2]|max_length[15]|is_not_unique[course.id]",
        "teacher" => "required|alpha_numeric|is_not_unique[teacher.id]"
    ];

    public $subject_errors = [
        "subjectCode" => [
            "required" => "Missing course code",
            "is_unique" => "Course already exits with the same code"
        ],
        "subjectName" => [
            "required" => "Missing subject name",
            "max_length" => "Course Name too long"
        ],
        "course" => [
            "required" => "Missing course name",
            "is_not_unique" => "Course not present in database"
        ],
        "teacher" => [
            "required" => "Missing teacher name",
            "is_not_unique" => "Teacher not present in database"
        ]
    ];
}
