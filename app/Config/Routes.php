<?php namespace Config;

// Create a new instance of our RouteCollection class.

$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index', [ "as" => "index" ]);
$routes->get("/media", "Home::media", [ "as" => "media" ]);
$routes->get("/forget-password", "Home::forgetPassword", [ "as" => "forget_password" ]);
$routes->group("/admin", function ($routes) {
    $routes->add("/", "Admin::index", [ "as" => "admin_index" ]);
    $routes->add("dashboard", "Admin::dashboard", [ "as" => "admin_dashboard" ]);
    $routes->add("login", "Admin::login", [ "as" => "admin_login" ]);
    $routes->add("logout", "Admin::logout", [ "as" => "admin_logout" ]);
    $routes->add("set-password", "Admin::setPassword", [ "as" => "admin_set_password" ]);
    $routes->add("change-password", "Admin::changePassword", [ "as" => "admin_change_password" ]);
    $routes->group("api", function ($routes) {
        $routes->add("teachers", "Admin::teachers", [ "as" => "admin_api_teachers" ]);
        $routes->add("routine", "RoutineAdmin::routineFetch", [ "as" => "admin_api_routine" ]);
    });

    $routes->group("student", function ($routes) {
        $routes->add("/", "StudentAdmin::index", [ "as" => "admin_student_dashboard" ]);
        $routes->add("add", "StudentAdmin::studentAdd", [ "as" => "admin_student_add" ]);
        $routes->add("edit", "StudentAdmin::studentModify", [ "as" => "admin_student_edit" ]);
        $routes->add("delete", "StudentAdmin::studentDelete", [ "as" => "admin_student_delete" ]);
    });

    $routes->group("teacher", function($routes) {
        $routes->add("/", "TeacherAdmin::index", [ "as" => "admin_teacher_dashboard" ]);
        $routes->add("add", "TeacherAdmin::teacherAdd", [ "as" => "admin_teacher_add" ]);
        $routes->add("edit", "TeacherAdmin::teacherModify", [ "as" => "admin_teacher_edit" ]);
        $routes->add("delete", "TeacherAdmin::teacherDelete", [ "as" => "admin_teacher_delete" ]);
    });

    $routes->group("course", function ($routes) {
        $routes->add("/", "CourseAdmin::index", [ "as" => "admin_course_dashboard" ]);
        $routes->add("add", "CourseAdmin::courseAdd", [ "as" => "admin_course_add" ]);
        $routes->add("edit", "CourseAdmin::courseModify", [ "as" => "admin_course_edit" ]);
    });

    $routes->group("batch", function ($routes) {
        $routes->add("/", "BatchAdmin::index", [ "as" => "admin_batch_dashboard" ]);
        $routes->add("add", "BatchAdmin::batchAdd", [ "as" => "admin_batch_add" ]);
    });

    $routes->group("subject", function ($routes) {
        $routes->add("/", "SubjectAdmin::index", [ "as" => "admin_subject_dashboard" ]);
        $routes->add("add", "SubjectAdmin::subjectAdd", [ "as" => "admin_subject_add" ]);
        $routes->add("edit", "SubjectAdmin::subjectModify", [ "as" => "admin_subject_edit" ]);
    });

    $routes->group("routine", function ($routes) {
        $routes->add("edit-routine", "RoutineAdmin::routineModify", [ "as" => "admin_routine_edit" ]);
        $routes->add("update-routine", "RoutineAdmin::routineUpdate", [ "as" => "admin_routine_update" ]);
        $routes->add("populate", "RoutineAdmin::routinePopulate", [ "as" => "admin_routine_populate" ]);
        $routes->add("dashboard", "RoutineAdmin::routineDashboard", [ "as" => "admin_routine_dashboard" ]);
    });
});
$routes->group("student", function ($routes) {
    $routes->group("api", function ($routes) {
        $routes->add("classes", "Student::getClasses", [ "as" => "student_api_classes" ]);
        $routes->add("class", "Student::getClass", [ "as" => "student_api_class" ]);
        $routes->add("attend", "Student::attendanceAdd", [ "as" => "student_api_attend" ]);
        $routes->add("notes", "Student::fetchNotes", [ "as" => "student_api_notes" ]);
    });
    $routes->add("/", "Student::index", [ "as" => "student_index" ]);
    $routes->add("dashboard", "Student::dashboard", [ "as" => "student_dashboard" ]);
    $routes->add("login", "Student::login", [ "as" => "student_login" ]);
    $routes->add("set-password", "Student::setPassword", [ "as" => "student_set_password" ]);
    $routes->add("change-password", "Student::changePassword", [ "as" => "student_change_password" ]);
    $routes->add("logout", "Student::logout", [ "as" => "student_logout" ]);
    $routes->add("notices", "Student::notices", [ "as" => "student_notices" ]);
    $routes->add("notice", "Student::notice", [ "as" => "student_notice" ]);
    $routes->add("your-classes", "Student::routine", [ "as" => "student_routine" ]);
    $routes->add("set-picture", "Student::pictureAdd", [ "as" => "student_picture_set" ]);
});
$routes->group("teacher", function ($routes) {
    $routes->group("api", function ($routes) {
        $routes->add("schedule", "Teacher::getRoutine", [ "as" => "teacher_schedule" ]);
        $routes->add("routine", "Teacher::showRoutine", [ "as" => "teacher_api_routine" ]);
    });
    $routes->add("/", "Teacher::index", [ "as" => "teacher_index" ]);
    $routes->add("dashboard", "Teacher::dashboard", [ "as" => "teacher_dashboard" ]);
    $routes->add("login", "Teacher::login", [ "as" => "teacher_login" ]);
    $routes->add("logout", "Teacher::logout", [ "as" => "teacher_logout" ]);
    $routes->add("set-password", "Teacher::setPassword", [ "as" => "teacher_set_password" ]);
    $routes->add("change-password", "Teacher::changePassword", [ "as" => "teacher_change_password" ]);
    $routes->add("notify", "Teacher::noticeAdd", [ "as" => "teacher_notify" ]);
    $routes->add("notices", "Teacher::notices", [ "as" => "teacher_notices"]);
    $routes->add("delete-notice", "Teacher::noticeDelete", [ "as" => "teacher_notice_delete" ]);
    $routes->add("edit-notice", "Teacher::noticeModify", [ "as" => "teacher_notice_modify" ]);
    $routes->add("your-classes", "Teacher::getRoutine", [ "as" => "teacher_routine" ]);
    $routes->add("add-class", "Teacher::classAdd", [ "as" => "teacher_class_add" ]);
    $routes->get("attendance-records", "Teacher::attendanceRecord", [ "as" => "teacher_records" ]);
    $routes->add("set-picture", "Teacher::pictureAdd", [ "as" => "teacher_picture_set" ]);
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
