<?php
namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Database\BaseBuilder;
use Config\Database;


class teacher extends BaseController {
    use ResponseTrait;
    public function __construct() {
        $this->db = model("App\Models\Teacher");
        $this->session_key = "teacher";
    }

    public function index() {
        // Check if a valid teacher session is in progress
        // If it is then redirect to the teacher's dashboard or show login page
        if ($this->LoggedIn())  return $this->redirect(route_to("teacher_dashboard"));

        // If we've been redirected back from the /teacher/login, then that means there were error with login
        // Get those error messages from the session data and add it to the view render to show them 
        $error = $this->session->getFlashdata("error");
        if (empty($error))  $error = "";

        return view('teacher/login', [
            "errorMSG" => $error
        ]);
    }

    public function login() {
        // For safety reason, re-initialize the entire session data
        $this->session->stop();
        log_message("debug", "[App\Controller\Teacher] Recreated a new session for login {$this->request->getIPAddress()}");

        // This method is only for POST requests. Others are not handled
        if ($this->request->getMethod(true) !== "POST") {
            log_message(
                "debug",
                "[App\Controllers\Teacher] Invalid method {$this->request->getMethod(true)} for /teacher/login"
            );
            return $this->redirect(route_to("teacher_index"));
        }

        $teacherID = $this->request->getPost("username");
        $password = $this->request->getPost("password");

        // Use the QueryBuilder to build queries and get the teacher record from the database.
        // This returns an array of objects containing the records that marches the teacher ID. SHOULD ALWAYS BE 1 RECORD PER QUERY
        $teacher = $this->db->getWhere([ "id" => $teacherID ])->getRow();

        // A basic check to see if the input teacher ID is valid ID or not.
        // If not then redirect to the login form with the appropriate error message
        if (empty($teacher)) {
            $this->session->setFlashdata("error", "Wrong Teacher ID");
            return $this->redirect(route_to("teacher_index"));
        }

        // If password verification fails, then set the error message and redirect to login page
        if (password_verify($password, $teacher->password) === false) {
            $this->session->setFlashdata("error", "Wrong password");
            return $this->redirect(route_to("teacher_index"));
        }

        // Since teachers can be marked as deleted rather than being removed from database, a login attempt to such a account must be disallowed
        if ($teacher->left_at !== null) {
            $this->session->setFlashdata("error", "Account disabled");
            return $this->redirect(route_to("teacher_index"));
        }

        // Since the passwords are set by the admin during the teacher record creation,
        // Allow the teacher to create their own password during the first login of their account
        if ($password === "teacher") {
            $this->session->set("id", $teacherID);
            return $this->redirect(route_to("teacher_set_password"));
        }

        // Mark in the session data that the student has logged in properly
        $this->session->set($this->session_key, $teacher->id);
        log_message("debug", "[App\Controller\Teacher] {$teacher->id} successfully logged in with session " . session_id());

        return $this->redirect(route_to("teacher_dashboard"));
    }

    public function setPassword() {
        // A check to see if the student tried a login before being redirected to /student/set-password.
        // Allows us to not require to send student ID using the URL because the state is saved in the SERVER session files
        if ($this->LoggedIn())    return $this->redirect(route_to("teacher_index"));
        $teacherID = $this->session->get("id");

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controllers\Teacher] Trying to set password for $teacherID. Triggered By " . $this->request->getIPAddress()
            );

            if ($this->validate("password") === false) {
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect(route_to("teacher_set_password"));
            }

            $password = $this->request->getPost("password");
            $this->db->update($teacherID, [ "password" => $password ]);

            $this->session->remove($this->session_key);
            log_message(
                "alert",
                "[App\Controllers\Teacher] Teacher $teacherID password updated. Triggered By " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("teacher_index"));
        }

        // When the /student/set-password is called via GET method, then log the message as alert
        // Check for validation errors for any previous POST submissions and render the view
        log_message(
            "debug",
            "[App\Controllers\Teacher] /teacher/set-password called for teacher $teacherID"
        );

        $validation = $this->session->getFlashdata("validation");

        return view("create_password", [
            "validation" => $validation
        ]);
    }

    public function dashboard() {
        if ($this->LoggedIn() === false)   return $this->redirect(route_to("teacher_index"));

        $teacher = $this->session->get($this->session_key);
        // Fetch the teacher's data from the database and pass it to the view for rendering
        $teacher_data = $this->db->builder()
                                 ->select("id, name, picture")
                                 ->where([ "id" => $teacher ])
                                 ->get();
        
        log_message(
            "debug",
            "[App\Controllers\Teacher] Fetched $teacher data from the database. Rendering the view"
        );

        if (empty($teacher_data)) {
            // If the teacher data response is empty even after using the session data, then the session data is probably corrupted.
            log_message(
                "alert",
                "[App\Controller\Teacher] Authenticated teacher $teacher data not found in the database."
            );

            return $this->response->setStatusCode(404);
        }

        return view("teacher/dashboard", $teacher_data->getRowArray());
    }

    public function pictureAdd() {
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "POST")  return $this->failForbidden();
        $picture = $this->saveFile("picture");
        $teacher = $this->session->get($this->session_key);
        $this->db->update($teacher, [ "picture" => $picture ]);
        log_message(
            "debug",
            "[App\Controllers\Teacher] Successfully changed picture for teacher " . $this->session->get($this->session_key)
        );

        return $this->redirect(route_to("teacher_index"));
    }

    public function noticeAdd() {
        if ($this->LoggedIn() === false)   return $this->redirect(route_to("teacher_index"));
        $teacher = $this->session->get($this->session_key);

        if ($this->request->getMethod(true) === "POST") {
            // When the /teacher/notify is called via POST method, then validate the data along the rules
            // And try to add the notice to the database if the data is valid, or redirect with validation erros set
            log_message(
                "debug",
                "[App\Controllers\Teacher] /teacher/notify received data for a new notice from teacher $teacher"
            );

            // If the validation fails, then it may be because of improper data and/or malicious data.
            // Log a message for the admin for the offending teacher
            if ($this->validate("notice") === false) {
                log_message(
                    "alert",
                    "[App\Controllers\Teacher] Invalid data was notice data was sent by teacher $teacher"
                );
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect(route_to("teacher_notify"));
            }

            // Prepare the notice data for insertion. The file field is handled separately
            // as it needs some processing before being written to the database
            $notice_data = [
                "title" => $this->request->getPost("title", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                "for_batch" => $this->request->getPost("batch"),
                "issued_by" => $teacher,
                "text" => empty($this->request->getPost("text"))? null: $this->request->getPost("text", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                "file" => $this->saveFile("file")
            ];

            $notice = model("App\Models\Notice");
            $notice->insert($notice_data);
            log_message(
                "debug",
                "[App\Controllers\Teacher] New notice created by teacher $teacher"
            );

            return $this->redirect(route_to("teacher_notices"));
        }

        // When the /teacher/notify is called via GET method, then log the message as alert
        // Check for validation errors for any previous POST submissions and render the view
        log_message(
            "debug",
            "[App\Controllers\Teacher] /teacher/notify called by teacher $teacher"
        );

        $validation = $this->session->getFlashdata("validation");

        return view("teacher/issue_notice", [
            "validation" => $validation
        ]);
    }

    public function notices() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("teacher_index"));

        $teacher = $this->db->find($this->session->get($this->session_key));
        $notices_db = model("App\Models\Notice");
        $notices = $notices_db->where("issued_by", $teacher->id)->findAll();

        return view("teacher/notices", [ "notices" => $notices ]);
    }

    public function logout() {
        // Clear the session data, regenerate the id and redirect to the homepage
        $this->session->destroy();
        return $this->redirect(route_to("index"));
    }

    public function changePassword() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("teacher_index"));
        $teacher = $this->session->get($this->session_key);

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controller\Teacher] Trying to change $teacher password. Triggered by ". $this->request->getIPAddress()
            );

            /**
             * Since we are updating the current password, so first we need to make sure that the student knows the password
             * After that, we need to update the record for the new password
             */
            $current_password = $this->request->getPost("currpassword");
            $password_record = $this->db->find($teacher);
            if (password_verify($current_password, $password_record->password) === false) {
                $this->session->setFlashdata("validation", [ "Wrong password" ]);
                return $this->redirect(route_to("teacher_change_password"));
            }

            if ($this->validate("password") === false) {
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect(route_to("teacher_change_password"));
            }
            $password = $this->request->getPost("password");
            $this->db->update($teacher, [ "password" => $password ]);

            return $this->redirect(route_to("teacher_logout"));
        }

        log_message(
            "debug",
            "[App\Controller\Teacher] Password change for $teacher requested by " . $this->request->getIPAddress()
        );
        $validation = $this->session->getFlashdata("validation");

        return view("change_password", [ "validation" => $validation ]);
    }

    public function noticeDelete() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("teacher_index"));
        if ($this->request->getMethod(true) === "POST") {
            $notice = $this->request->getPost("notice");
            $notices = model("App\Models\Notice");

            $notice_record = $notices->find($notice);

            if ((is_null($notice_record) === false) && ($notice_record->issued_by === $this->session->get($this->session_key))) {
                $notices->delete($notice);
                log_message(
                    "debug",
                    "[App\Controllers\Teacher] Notice $notice successfully deleted by {$this->request->getIPAddress()}"
                );
            } else {
                log_message(
                    "alert",
                    "[App\Controllers\Teacher] Teacher {$this->session->get($this->session_key)} tried to delete notice $notice issued by teacher {$notice_record->issued_by}"
                );
            }

            return $this->redirect($this->request->getServer("HTTP_REFERER", FILTER_SANITIZE_URL));
        }

        return $this->response->setStatusCode(404);
    }

    public function noticeModify() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("teacher_index"));
        $notice_db = model("App\Models\Notice");
        $notice_record = $notice_db->select("id, title, text, for_batch, issued_by")->find($this->request->getGet("notice"));

        if ($notice_record->issued_by !== $this->session->get($this->session_key))  return $this->response->setStatusCode(403);

        if ($this->request->getMethod(true) === "POST") {
            var_dump($this->request->getPost());
            var_dump($this->request->getFiles());
            // When the /teacher/edit-notice is called via POST method, then validate the data along the rules
            // And try to add the notice to the database if the data is valid, or redirect with validation errors set
            log_message(
                "debug",
                "[App\Controllers\Teacher] Trying to modify notice {$notice_record->id} by {$this->session->get($this->session_key)}"
            );

            // If the validation fails, then it may be because of improper data and/or malicious data.
            // Log a message for the admin for the offending teacher
            if ($this->validate("notice") === false) {
                log_message(
                    "alert",
                    "[App\Controllers\Teacher] Invalid data was notice data was sent by teacher {$this->session->get($this->session_key)}"
                );
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            // Prepare the notice data for insertion. The file field is handled separately
            // as it needs some processing before being written to the database
            $notice_data = [
                "title" => $this->request->getPost("title", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                "for_batch" => $this->request->getPost("batch"),
                "text" => empty($this->request->getPost("text"))? null: $this->request->getPost("text", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
            ];
            if ($this->request->getFile("file")->isValid())   $notice_data["file"] = $this->saveFile("file");
            $notice_db->update($notice_record->id, $notice_data);

            return $this->redirect(route_to("teacher_notices"));
        }

        $validation = $this->session->getFlashdata("validation");
        return view("teacher/issue_notice", [
            "notice" => $notice_record,
            "validation" => $validation
        ]);
    }

    public function getRoutine() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("teacher_index"));

        if ($this->request->isAJAX()) {
            $class_db = Database::connect()->table("class_routine");
            $query = $class_db->select("class_routine.subject_id, subject.name")
                              ->where("teacher", $this->session->get($this->session_key))
                              ->join("subject", "class_routine.subject_id = subject.code")
                              ->get()->getResult();

            return $this->response->setJSON($query);
        }

        return view("teacher/class", [ "teacher" => $this->session->get($this->session_key) ]);
    }

    public function showRoutine() {
        if ($this->LoggedIn() === false || $this->request->isAJAX() === false)  return $this->failForbidden();
        $subject = $this->request->getGet("subject", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $class_db = Database::connect()->table("class_routine");
        $query = $class_db->select("routine.batch, routine.day, class_routine.id")
                          ->where("subject_id", $subject)
                          ->where("teacher", $this->session->get($this->session_key))
                          ->join("routine", "class_routine.routine_id = routine.id")
                          ->get();

        return $this->response->setJSON($query->getResult());
    }

    public function classAdd() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("teacher_index"));

        if ($this->request->getMethod(true) === "POST") {
            $routine_id = $this->request->getGet("routine", FILTER_SANITIZE_NUMBER_INT);
            $class_name = $this->request->getPost("title", FILTER_SANITIZE_STRING);
            $lecture = $this->saveFile("lecture");
            $note = $this->saveFile("note");

            $class_db = Database::connect()->table("class");
            $resources_db = Database::connect()->table("class_resources");
            $attendance_db = Database::connect()->table("attendance");

            // First save the data for the class in the DB to get the class ID.
            $class_data = [
                "topic" => $class_name,
                "taught_by" => $this->session->get($this->session_key),
                "routine_id" => $routine_id,
                "week" => date("W", time())
            ];
            $classID = $class_db->insert($class_data)->connID->insert_id;
            $resources_data = [
                "for_class" => $classID,
                "resource" => $lecture,
                "type" => "lecture",
                "name" => $class_name
            ];
            $resources_db->insert($resources_data);
            if (is_null($note) === false) {
                $resources_data = [
                    "for_class" => $classID,
                    "resource" => $note,
                    "type" => "note",
                    "name" => $class_name . "_note"
                ];
                $resources_db->insert($resources_data);
            }
            $attendance_data = [
                "day" => date("c", time()),
                "teacher" => $this->session->get($this->session_key),
                "class_id" => $classID
            ];
            $attendance_db->insert($attendance_data);

            return $this->redirect(route_to("teacher_routine"));
        }

        return view("teacher/add_class");
    }


    public function attendanceRecord() {
        /**
         * Mapped to /teacher/attendance-records, it is called with the routine id of a subject
         * It fetches attendance records for that class during the current week and renders the view to be displayed.
         * If no class was/is taken for that routine id during current week, it just renders the view with a empty table
         */
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("teacher_index"));

        $attendance_db = Database::connect()->table("attendance_record");
        $class_db = Database::connect()->table("class");
        $routine = $this->request->getGet("routine", FILTER_SANITIZE_NUMBER_INT);
        $week = date("W");

        if ($class_db->where("routine_id", $routine)->where("week", $week)->countAllResults() < 1)
            return view("teacher/attendance_record", [ "records" => [] ]);

        $week_class = $class_db->select("id")->where("routine_id", $routine)
                               ->where("week", $week)->get()->getRow(1, 'object');

        $records = $attendance_db->select("student.name, attendance_record.joined")
            ->where("day", function (BaseBuilder $builder) use ($week_class) {
                return $builder->select("id")->from("attendance")->where("class_id", $week_class->id);
            })
            ->join("student", "attendance_record.student = student.id")->get();

        return view("teacher/attendance_record", [ "records" => $records->getResult() ]);
    }
}
