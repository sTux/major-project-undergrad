<?php
namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Controller;


class api extends Controller {
    use ResponseTrait;
    private ?\CodeIgniter\Database\MySQLi\Connection $db = null;

    public function __construct() {
        // Since this is a controller for APIs, therefore it will work with many different tables
        // So to avoid re-initialising models at every request, initialize it as generic connection to DB
        $this->db = \Config\Database::connect();
        helper("text");
    }

    public function _remap($method, ...$params) {
        $httpM = $this->request->getMethod(true);
        $isAjax = $this->request->isAjax();
        log_message(
            "debug",
            "[App\Controller\Api] Called the $method endpoint with $httpM Is AJAX? " . var_export($isAjax, true)
        );

        if (($httpM !== "GET") || ($this->request->isAjax() === false)) {
            return $this->failForbidden("UNSUPPORTED METHOD");
        }

        return $this->$method(...$params);
    }

    public function courses() {
        // Get a QueryBuilder instance over the `course` table
        $queryBuilder = $this->db->table("course");
        // Get a instance of the default View parser. Used for inline HTML rendering
        $parser = \Config\Services::parser();
        
        // Inline HTML template for the options available
        $template = "<option value=\"{id}\"> {name} </option>";
        $response_text = "";
        // Use the QueryBuilder instance to get all the courses avaliable in our database and return the results array
        $courses = $queryBuilder->get()->getResult();

        foreach ($courses as $course) {
            // During each iteration fetch a new row from the result array
            // and map the fields in thre result object to the corresponding keys 
            // that will be used during the rendering of the inline HTML template

            $response_text .= $parser->setData([
                "id" => $course->id,
                "name" => entities_to_ascii($course->name)
            ])->renderString($template);
        }

        $response_text = "<option disabled selected value> -- Select a course -- </option>" . $response_text;
        // Since we are retuning a string of <option>s, set the response as XML and respond with the rendered view
        return $this->setResponseFormat("xml")->respond($response_text);
    }

    public function batch($course) {
        // Get a QueryBuilder instance over the `batch` table and get the batches
        $queryBuilder = $this->db->table("batch");
        $batches = $queryBuilder->getWhere([
            "course" => $course
        ])->getResult();

        // Get the default View parser
        $parser = \Config\Services::parser();

        // Inline HTML template for options available
        $template = "<option value=\"{id}\"> {id} </option>";
        $response_text = "";

        // Iteratively build the options HTML
        foreach ($batches as $batch) {
            $response_text .= $parser->setData([
                "id" => $batch->id
            ])->renderString($template);
        }

        $response_text = "<option disabled selected value> -- Select a batch -- </option>" . $response_text;
        // Return a successful XML response
        return $this->setResponseFormat("xml")->respond($response_text);
    }

    public function subjects() {
        // Get a QueryBuilder instance over the `subject` table and get the subjects
        $builder = $this->db->table("subject");
        // Get the GET parameters from the request and sanitize for only strings
        $subject = $this->request->getGet("course", FILTER_SANITIZE_STRING);
        $teacher = $this->request->getGet("teacher", FILTER_SANITIZE_STRING);
        // Build query with a SELECT clause to select subject name and code
        $query = $builder->select("code, name");

        // Now add WHERE clauses based on GET parameters. Since a empty parameter can result in NULL being set in WHERE clause
        // That's why we need to check if each of the parameters are set
        if (empty($subject) === false)
            $query = $query->where("part_of", $subject);
        if (empty($teacher) === false)
            $query = $query->where("taught_by", $teacher);
        
        // Now complete the query and get the array of objects for each records that matched
        $query = $query->get();
        $subjects = $query->getResult();
        
        // Get the default View parser
        $parser = \Config\Services::parser();

        // Inline HTML template for options available
        $template = "<option value=\"{id}\"> {name} </option>";
        $response_text = "";

        // Iteratively build the options HTML
        foreach ($subjects as $subject) {
            $response_text .= $parser->setData([
                "id" => $subject->code,
                "name" => entities_to_ascii($subject->name)
            ])->renderString($template);
        }

        $response_text = "<option disabled selected value> -- Select a subject -- </option>" . $response_text;
        // Return a successful XML response
        return $this->setResponseFormat("xml")->respond($response_text);
    }
}
