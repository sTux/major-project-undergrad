<?php
namespace App\Controllers;


class SubjectAdmin extends BaseController {
    public function __construct() {
        $this->session_key = "loggedIn";
        $this->db = model("App\Models\Subject");
    }

    public function index() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));
        $fields = "code, subject.name, part_of AS course_code, course.name AS course_name, taught_by AS teacher_id, teacher.name AS teacher_name";
        $query = $this->db->select($fields)
                      ->join("teacher", "teacher.id = subject.taught_by", "INNER")
                      ->join("course", "course.id = subject.part_of", "INNER");
        $course = $this->request->getVar("course", FILTER_SANITIZE_STRING);
        $teacher = $this->request->getVar("teacher", FILTER_SANITIZE_STRING);

        if (is_null($course) === false) $query = $query->where("subject.part_of", $course);
        if (is_null($teacher) === false) $query = $query->where("subject.taught_by", $teacher);

        return view("admin/dashboard_subject", [ "subjects" => $query->findAll() ]);
    }

    public function subjectAdd() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));

        if ($this->request->getMethod(true) === "GET") {
            log_message(
                "debug",
                "[App\Controllers\SubjectAdmin] New subject being added by " . $this->request->getIPAddress()
            );

            $validation = $this->session->getFlashdata("validation");
            return view("admin/add_subject", [
                "validation" => $validation
            ]);
        }

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controller\SubjectAdmin] Got data for new subject from " . $this->request->getIPAddress()
            );

            if ($this->validate("subject") === false) {
                log_message(
                    "alert",
                    "[App\Controller\SubjectAdmin] Invalid subject data received from " . $this->request->getIPAddress()
                );
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            $subject_data = [
                "code" => $this->request->getPost("subjectCode", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP),
                "name" => $this->request->getPost("subjectName", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP),
                "part_of" => $this->request->getPost("course", FILTER_SANITIZE_STRING),
                "taught_by" => $this->request->getPost("teacher", FILTER_SANITIZE_STRING)
            ];
            $this->db->insert($subject_data);

            log_message(
                "alert",
                "[App\Controllers\SubjectAdmin] {$subject_data["code"]} course successfully added by " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("admin_subject_dashboard"));
        }
    }

    public function subjectModify() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));
        $subject = $this->request->getGet("subject", FILTER_SANITIZE_STRING);

        if ($this->request->getMethod(true) === "GET") {
            log_message(
                "debug",
                "[App\Controller\SubjectAdmin] Trying to modify the subject record for $subject"
            );

            $validation = $this->session->getFlashdata("validation");
            return view("admin/modify_subject", [
                "validation" => $validation
            ]);
        }

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controller\SubjectAdmin] Got data to update subject $subject from " . $this->request->getIPAddress()
            );
            $validation = [
                "subjectName" => "permit_empty|alpha_space|max_length[500]",
                "teacher" => "permit_empty|alpha_numeric|is_not_unique[teacher.id]"
            ];
            $validation_errors = [
                "subjectName" => [
                    "max_length" => "Subject Name too long",
                    "alpha_space" => "Subject Name contains illegal characters"
                ],
                "teacher" => [
                    "is_not_unique" => "Teacher not present in database",
                    "alpha_numeric" => "Subject Name contains illegal characters"
                ]
            ];

            if ($this->validate($validation, $validation_errors) === false) {
                log_message(
                    "alert",
                    "[App\Controller\SubjectAdmin] Got invalid data for subject $subject from " . $this->request->getIPAddress()
                );

                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            $data = [];
            if (empty($this->request->getPost("subjectName")) === false)
                $data["name"] = $this->request->getPost("subjectName", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP);
            if (empty($this->request->getPost("teacher")) === false)
                $data["taught_by"] = $this->request->getPost("teacher",FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP);

            $this->db->update($subject, $data);

            log_message(
                "debug",
                "[App\Controller\SubjectAdmin] Updated subject data $subject with data from " . $this->request->getIPAddress()
            );

            return $this->redirect(route_to("admin_subject_dashboard"));
        }
    }
}