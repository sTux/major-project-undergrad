<?php
namespace App\Controllers;


class StudentAdmin extends BaseController {
    public function __construct() {
        $this->db = model("App\Models\Student");
        $this->session_key = "loggedIn";
    }

    public function index() {
        if ($this->LoggedIn() === false)     return $this->redirect(route_to("admin_index"));

        $students = $this->db->select("id, name, batch")->findAll();

        return view("admin/dashboard_student", [
            "students" => $students,
            "partial" => view("delete_popup", [ "delete_url" => "/admin/student/delete"])
        ]);
    }

    public function studentAdd() {
        // Make sure the admin is currently logged-in before trying to add any kind of data to the database
        if ($this->LoggedIn() === false)     return $this->redirect(route_to("admin_index"));

        // When called via the HTTP POST method, validate the data and either add to database or store the validation errors and redirect
        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controllers\Admin] Got student data from /admin/add-student from " . $this->request->getIPAddress()
            );

            // Alert the admin about possibly corrupt and malicious data being inserted into the database
            if ($this->validate("student") === false) {
                log_message(
                    "alert",
                    "[App\Controllers\Admin] Invalid student data sent from " . $this->request->getIPAddress()
                );

                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            // Data has been validated against the rules. Now create the data array for QueryBuilder to insert into database.
            // Since picture and password are set by the student, set them as null in the data array
            $student_data = [
                "id" => $this->request->getVar("id", FILTER_SANITIZE_STRING),
                "name" => $this->request->getVar("name", FILTER_SANITIZE_STRING),
                "batch" => $this->request->getVar("batch"),
                "password" => "student",
                "picture" => null
            ];

            $this->db->insert($student_data);

            // Now that a new student data has been added, log the success message in the logs for the admin with the new student's ID.
            // And then redirect to the student listings for the admin
            log_message(
                "alert",
                "[App\Controllers\Admin] Created a new student entry {$this->request->getVar("id")}. Triggered from " . $this->request->getIPAddress()
            );

            return $this->redirect(route_to("admin_student_dashboard"));
        }

        // When called using HTTP GET method, get all the validation errors from the session's flash data and render the form
        log_message(
            "debug",
            "[App\Controllers\Admin] Trying to add a new student into the database. Triggered by " . $this->request->getIPAddress()
        );

        $validation = $this->session->getFlashdata("validation");

        return view("admin/add_student", [
            "validation" => $validation
        ]);
    }

    public function studentModify() {
        if ($this->LoggedIn() === false)   return $this->redirect(route_to("admin_index"));
        $student = $this->request->getGet("student");

        // When the method called with a POST method, then validate the data and update the database
        if ($this->request->getMethod(true) === "POST") {
            $validation = [
                "name" => "permit_empty|alpha_space",
                "batch" => "permit_empty|alpha_numeric"
            ];

            if ($this->validate($validation) === false) {
                log_message(
                    "alert",
                    "[App\Controllers\Admin] Invalid update blocked for student $student. From " . $this->request->getIPAddress()
                );

                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            // Now that data has been validated, move to create a data array from the form data.
            // This one takes a lot of if's because each update request may update only a few fields
            $data = [];
            if (empty($this->request->getPost("name")) === false)
                $data["name"] = $this->request->getPost("name", FILTER_SANITIZE_STRING);
            if ($this->request->getPost("batch") !== null)
                $data["batch"] = $this->request->getPost("batch", FILTER_SANITIZE_STRING);
            if ($this->request->getPost("password-empty") === "on")
                $data["password"] = "student";

            $this->db->update($student, $data);

            log_message(
                "alert",
                "[App\Controllers\Admin] Student details updated for $student from " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("admin_student_dashboard"));
        }

        // When the method called with a GET method, then fetch the validation errors and render the view
        log_message(
            "debug",
            "[App\Controllers\Admin] Trying to modify student record for $student."
        );

        $validation = $this->session->getFlashdata("validation");
        return view("admin/modify_student", [
            "validation" => $validation
        ]);
    }

    public function studentDelete() {
        /**
         * Mapped to /admin/student/delete. It is uses a HTTP POST data about the student to delete and marks the student as left from the system
         */
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "POST")  return $this->redirect(route_to("admin_index"));

        $student = $this->request->getPost("student", FILTER_SANITIZE_STRING);
        $this->db->delete($student);

        return $this->redirect(route_to("admin_student_dashboard"));
    }
}
