<?php
namespace App\Controllers;


class TeacherAdmin extends BaseController {
    public function __construct() {
        $this->db = model("App\Models\Teacher");
        $this->session_key = "loggedIn";
    }

    public function index() {
        if ($this->LoggedIn() === false) return $this->redirect(route_to("admin_index"));

        $teachers = $this->db->select("id, name")->findAll();

        return view("admin/dashboard_teacher", [
            "teachers" => $teachers,
            "partial" => view("delete_popup", [ "delete_url" => "/admin/teacher/delete"])
        ]);
    }

    public function teacherAdd() {
        // Make sure the admin is currently logged-in before trying to add any kind of data to the database
        if ($this->LoggedIn() === false) return $this->redirect(route_to("admin_index"));

        // When called via the HTTP POST method, validate the data and either add to database or store the validation errors and redirect
        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controllers\Admin] Got teacher data from /admin/add-teacher from " . $this->request->getIPAddress()
            );

            // Alert the admin about possibly corrupt and malicious data being inserted into the database
            if ($this->validate("teacher") === false) {
                log_message(
                    "alert",
                    "[App\Controllers\Admin] Invalid teacher data sent from " . $this->request->getIPAddress()
                );

                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            // Data has been validated against the rules. Now create the data array for QueryBuilder to insert into database.
            // Since picture and password are set by the teacher, set them as null in the data array
            $teacher_data = [
                "id" => $this->request->getVar("id", FILTER_SANITIZE_STRING),
                "name" => $this->request->getVar("name", FILTER_SANITIZE_STRING),
                "password" => "teacher",
                "picture" => null
            ];

            $this->db->insert($teacher_data);

            // Now that a new teacher data has been added, log the success message in the logs for the admin with the new teacher's ID.
            // And then redirect to the teacher listings for the admin
            log_message(
                "alert",
                "[App\Controllers\Admin] Created a new teacher entry {$this->request->getVar("id")}. Triggered from " . $this->request->getIPAddress()
            );

            return $this->redirect(route_to("admin_teacher_dashboard"));
        }

        // When called using HTTP GET method, get all the validation errors from the session's flash data and render the form
        log_message(
            "debug",
            "[App\Controllers\Admin] Trying to add a new teacher into the database. Triggered by " . $this->request->getIPAddress()
        );

        $validation = $this->session->getFlashdata("validation");

        return view("admin/add_teacher", [
            "validation" => $validation
        ]);
    }

    public function teacherModify() {
        if ($this->LoggedIn() === false)   return $this->redirect(route_to("admin_index"));
        $teacher = $this->request->getGet("teacher");

        // When the method called with a POST method, then validate the data and update the database
        if ($this->request->getMethod(true) === "POST") {
            if ($this->validate([ "name" => "permit_empty|alpha_space" ]) === false) {
                log_message(
                    "alert",
                    "[App\Controllers\Admin] Invalid update blocked for teacher $teacher. From " . $this->request->getIPAddress()
                );

                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            // Now that data has been validated, move to create a data array from the form data.
            // This one takes a lot of if's because each update request may update only a few fields
            $data = [];
            if (empty($this->request->getVar("name")) === false)
                $data["name"] = $this->request->getVar("name", FILTER_SANITIZE_STRING);
            if ($this->request->getVar("password-empty") === "on")
                $data["password"] = "teacher";

            $this->db->update($teacher, $data);

            log_message(
                "alert",
                "[App\Controllers\Admin] Teacher details updated for $teacher from " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("admin_teacher_dashboard"));
        }

        log_message(
            "debug",
            "[App\Controllers\Admin] Trying to modify teacher record for $teacher."
        );

        // When the method called with a GET method, then fetch the validation errors and render the view
        $validation = $this->session->getFlashdata("validation");
        return view("admin/modify_teacher", [
            "validation" => $validation
        ]);
    }

    public function teacherDelete() {
        /**
         * Mapped to /admin/teacher/delete. It is uses a HTTP POST data about the teacher to delete and marks the student as left from the system
         */
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "POST")  return $this->redirect(route_to("admin_index"));

        $teacher = $this->request->getPost("teacher", FILTER_SANITIZE_STRING);
        $this->db->delete($teacher);

        return $this->redirect(route_to("admin_teacher_dashboard"));
    }
}