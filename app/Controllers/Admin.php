<?php
namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;


class admin extends BaseController {
    use ResponseTrait;

    public function __construct() {
        $this->db = model("App\Models\AdminPasswords");
        $this->session_key = "loggedIn";
    }

    public function teachers() {
        // This is a API endpoint meant only for the admin. Thus this endpoint will require a active admin session.
        if ($this->LoggedIn() === false)    return $this->failUnauthorized();

        // Now that we know a authorized admin session in progress, fetch teachers data from db and send it.
        $teacher_db = model("App\Models\Teacher");
        $parser = \Config\Services::parser();
        $response_template = "<option value=\"{id}\"> {name} </option>";

        $teachers = $teacher_db->select("id, name")->findAll();
        $response_text = "<option disabled selected value> -- Select a teacher -- </option>";

        foreach ($teachers as $teacher) {
            $response_text .= $parser->setData([
                "id" => $teacher->id,
                "name" => $teacher->name
            ])->renderString($response_template);
        }

        return $this->setResponseFormat("xml")->respond($response_text);
    }
    public function index() {
        /**
         * GET handler for the admin index page. Return the login page
         * It checks if the admin passwords database is empty or not.
         * It it's found to be empty then, directly navigate to the dashboard, otherwise render the admin login page
         **/
        if ($this->LoggedIn())  return $this->redirect(route_to("admin_dashboard"));

        $count = $this->db->countAllResults();

        if ($count === 0)
            return $this->redirect(route_to("admin_set_password"));
        
        // Before rendering the login form, check and see if we've any errors from previous login attempts
        $error = $this->session->getFlashdata("error");
        if ($error === null)  $error = "";
        if ($error === "The action you requested is not allowed.") $error = "Page expired. Resubmit data";

        return view('admin/login', [
            "errorMSG" => $error
        ]);
    }

    public function setPassword() {
        // Security check to see if passwords were already set for the admin account
        // If passwords were already set for the admin account then treat it as an error and redirect
        $passwordCount = $this->db->countAllResults();
        if ($passwordCount > 0) {
            // If the row count is more than 0, that means password was set. Redirect and ask the user to login via password
            // Also log the request in for future inspections for the administrators
            log_message(
                "alert",
                "[App\Controllers\Admin] /admin/set-password called after password initialisation was complete. Trigged from ". 
                $this->request->getIPAddress()
            );
            return $this->redirect(route_to("admin_index"));
        }

        if ($this->request->getMethod(true) == "GET") {
            // When the /admin/set-password is called via GET, log the message
            // Check for validation errors and render the view using the validation and other data
            log_message(
                "alert",
                "[App\Controllers\Admin] /admin/set-password called from " . $this->request->getIPAddress()
            );
            $validationErrors = $this->session->getFlashdata("validation");

            return view("create_password", [
                "allowUserName" => false,
                "validation" => $validationErrors
            ]);
        }

        if ($this->request->getMethod(true) === "POST") {
            // When the /admin/set-password is called via POST, then validate the form data
            // If validation fails, then set flash data and redirect back to /admin/set-password with validations errors set
            log_message(
                "debug",
                "[App\Controller\Admin] Trying to set admin's password. Triggered by ". $this->request->getIPAddress()
            );
            
            if ($this->validate("password") === false) {
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect(route_to("admin_set_password"));
            }
            
            $password = $this->request->getPost("password");
            $this->db->insert([
                "password" => $password
            ]);

            // Since a new password is set, remove the loggedIn session data and redirect to the /admin for fresh login
            $this->session->remove($this->session_key);
            
            log_message(
                "alert",
                "[App\Controllers\Admin] Admin password updated. Triggered By " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("admin_index"));
        }
    }

    public function login() {
        /**
         * POST handler for the admin login page.
         * If the method is not found to be POST, then the method will redirect to /admin/index instead
        **/
        // As a safety measure, destroy all the session cookies as soon as user tries to login here
        $this->session->stop();

        if ($this->request->getMethod(true) === "GET")
            return $this->redirect(route_to("admin_index"));

        // Get the current password from the database
        $password = $this->db->orderBy("created_at", "DESC")->first();
        $inputPassword = $this->request->getVar("password");

        if (isset($password->password) === false || password_verify($inputPassword, $password->password) === false) {
            log_message("debug", "[App\Controller\Admin] Failed login attempt for admin from " . $this->request->getIPAddress());
            // Redirect back to the login with a wrong password error with error message set in
            $this->session->setFlashdata("error", "Password doesn't match with admin password");
            return $this->redirect(route_to("admin_index"));
        }

        $this->session->set($this->session_key, true);
        return $this->redirect(route_to("admin_dashboard"));
    }

    public function dashboard() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));
        
        return view("admin/dashboard");
    }

    public function logout() {
        /**
         * Logout the admin and destroy the entire session data. No kind of session persistence is allowed for the admin
        **/
        $this->session->destroy();
        return $this->redirect(route_to("admin_index"));
    }

    public function changePassword() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controller\Admin] Trying to change admin's password. Triggered by ". $this->request->getIPAddress()
            );

            /**
             * Since we are updating the current password, so first we need to make sure that admin knows the password
             * After that, we need to mark the current password as deleted, then insert a new record for the password record
             * Also we need to make sure that the password doesn't match any of the last 5 password
             * And also that the table only contains maximum 6 records all the time
             */
            $current_password = $this->request->getPost("currpassword");
            $password_record = $this->db->orderBy("created_at", "DESC")->first();
            if (password_verify($current_password, $password_record->password) === false) {
                $this->session->setFlashdata("validation", [ "Wrong password" ]);
                return $this->redirect(route_to("admin_change_password"));
            }

            if ($this->validate("password") === false) {
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect(route_to("admin_change_password"));
            }
            // Try and insert the new password into the database. Look for errors from the database.
            // This error can report if a recently used password is getting re-used
            $password = $this->request->getPost("password");
            $this->db->insert([ "password" => $password ]);

            $this->db->delete($password_record->created_at, true);
            return $this->redirect(route_to("admin_logout"));
        }

        log_message(
            "alert",
            "[App\Controller\Admin] Request to change the admin password from " . $this->request->getIPAddress()
        );
        $validation = $this->session->getFlashdata("validation");

        return view("change_password", [ "validation" => $validation ]);
    }
}
