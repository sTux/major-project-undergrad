<?php
namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use Config\Database;


class RoutineAdmin extends BaseController {
    use ResponseTrait;
    public function __construct() {
        $this->session_key = "loggedIn";
        $this->db = model("App\Models\Routine");
    }

    public function routineDashboard() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));

        return view("admin/dashboard_routine");
    }

    public function routineFetch() {
        if ($this->LoggedIn() === false || $this->request->isAJAX() === false || $this->request->getMethod(true) !== "GET") return $this->failForbidden();
        $day = $this->request->getGet("day", FILTER_SANITIZE_STRING);
        $batch = $this->request->getGet("batch", FILTER_SANITIZE_STRING);
        $class_db = Database::connect()->table("routine_student");
        $routine_query = $this->db->select("id");
        if (!is_null($day)) $routine = $routine_query->where("day", $day);
        if (!is_null($batch)) $routine = $routine_query->where("batch", $batch);
        $routine = [];
        foreach ($routine_query->findAll() as $r)    array_push($routine, $r->id);
        if (empty($routine))   $routine = [ "NOPE" ];

        $classes = $class_db->whereIn("routine_id", $routine)->get()->getResult();

        return $this->setResponseFormat("json")->respond($classes);
    }

    public function routinePopulate() {
        /**
         * This method is mapped to /admin/routine/populate and is called right after a batch has been added using /admin/batch/add
         * This method gets the inserted batch id from the session flash data, and adds a routine data for each day of the week in a batch insert
         * After the batch insert is complete, get the redirect URL from the session flash data and redirect to the session's URL
         */
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));
        $batch = $this->session->getFlashdata("batch");
        $redirect = $this->session->getFlashdata("redirect");
        $days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        $data = [];

        if ($this->db->where("batch", $batch)->countAllResults() === 0) {
            foreach ($days as $day)
                array_push($data, [ "day" => $day, "batch" => $batch ]);
        }
        echo $this->db->builder->insertBatch($data);

        return $this->redirect($redirect);
    }

    public function routineModify() {
        /**
         * This method is mapped to /admin/routine/edit-routine and is used to add subjects to a existing routine record
         */
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "POST" || $this->request->isAJAX() === false)    return $this->failForbidden();
        $batch = $this->request->getPost("batch", FILTER_SANITIZE_STRING);
        $day = $this->request->getPost("day", FILTER_SANITIZE_STRING);

        $routine_id = $this->db->select("id")->where("batch", $batch)->where("day", $day)->first();
        $routine_id = $routine_id->id;

        $data = [
            "subject_id" => htmlentities($this->request->getPost("subject", FILTER_SANITIZE_STRING), ENT_QUOTES | ENT_HTML401 | ENT_SUBSTITUTE),
            "teacher" => htmlentities($this->request->getPost("teacher", FILTER_SANITIZE_STRING), ENT_QUOTES | ENT_HTML401 | ENT_SUBSTITUTE),
            "routine_id" => $routine_id,
        ];
        $classes = model("App\Models\ClassRoutine");
        $classes->insert($data);

        log_message(
            "debug",
            "[App\Controllers\RoutineAdmin] class {$classes->insertID()} added for routine {$routine_id}"
        );
        return $this->respondCreated(csrf_hash());
    }

    public function routineUpdate() {
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "POST" || $this->request->isAJAX() === false)    $this->failForbidden();

        $id = $this->request->getPost("id", FILTER_SANITIZE_NUMBER_INT);
        $subject = htmlentities($this->request->getPost("subject", FILTER_SANITIZE_STRING), ENT_QUOTES | ENT_HTML401 | ENT_SUBSTITUTE);
        $teacher = htmlentities($this->request->getPost("teacher", FILTER_SANITIZE_STRING), ENT_QUOTES | ENT_HTML401 | ENT_SUBSTITUTE);
        $data = [
            "subject_id" => $subject,
            "teacher" => $teacher
        ];
        $classes = model("App\Models\ClassRoutine");
        $classes->update($id, $data);

        return $this->respondCreated(csrf_hash());
    }
}