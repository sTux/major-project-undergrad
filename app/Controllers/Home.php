<?php
namespace App\Controllers;

class Home extends BaseController {
    public function __construct() {
        $this->db = model("App\Models\Files");
    }

    public function index() {
        return view('welcome_message');
    }

    public function media() {
        $media = sanitize_filename($this->request->getVar("name"));
        if (is_null($media))
            return $this->response->setStatusCode(404);

        $files = model("App\Models\File");
        $file = $files->select("mime, path")->find($media);

        if ($file->mime === "application/pdf")  $name = "notice.pdf";
        if ($file->mime === "video/mp4")    $name = "video.mp4";
        else $name = "media";

        return $this->response
                ->download($file->path, null, true)
                ->setFileName($name);
    }

    public function forgetPassword() {
        return view("forgetpassword");
    }
}
