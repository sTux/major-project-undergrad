<?php
namespace App\Controllers;

use CodeIgniter\Services;

class CourseAdmin extends BaseController {
    public function __construct() {
        $this->db = model("App\Models\Course");
        $this->session_key = "loggedIn";
    }

    public function courseAdd() {
        // Make sure the admin is currently logged in. If not redirect to login page
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));

        // When called using HTTP GET method, get all the validation errors from the session's flash data and render the form
        if ($this->request->getMethod(true) === "GET") {
            log_message(
                "debug",
                "[App\Controllers\CourseAdmin] Trying to add a new course in the database. Triggered by " . $this->request->getIPAddress()
            );

            $validation = $this->session->getFlashData("validation");

            return view("admin/add_course", [
                "validation" => $validation
            ]);
        }

        // When called via the HTTP POST method, validate the data and either add to database or store the validation errors and redirect
        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controllers\CourseAdmin] Got course data from /admin/add-course from " . $this->request->getIPAddress()
            );

            if ($this->validate("course") === false) {
                // Alert the admin about possibly corrupt and malicious data being inserted into the database
                log_message(
                    "alert",
                    "[App\Controllers\CourseAdmin] Got invalid course data sent from " . $this->request->getIPAddress()
                );

                $this->session->setFlashData("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            // Data has been validated against the rules. Now create the data array for QueryBuilder to insert into database.
            $course_data = [
                "id" => $this->request->getPost("id", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP),
                "name" => $this->request->getPost("name", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP)
            ];

            $this->db->insert($course_data);

            // Now that a new teacher data has been added, log the success message in the logs for the admin with the new teacher's ID.
            // And then redirect to the teacher listings for the admin
            log_message(
                "alert",
                "[App\Controllers\CourseAdmin] {$course_data["id"]} course successfully added by " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("admin_course_dashboard"));
        }
    }

    public function index() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));
        $courses = $this->db->select("id, name")->findAll();

        return view("admin/dashboard_course", [
            "courses" => $courses
        ]);
    }

    public function courseModify() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));
        $course = $this->request->getGet("course");

        if ($this->request->getMethod(true) === "GET") {
            log_message(
                "debug",
                "[App\Controllers\CourseAdmin] Trying to modify course $course Triggered by " . $this->request->getIPAddress()
            );

            $validation = $this->session->getFlashData("validation");
            return view("admin/modify_course", [
                "validation" => $validation
            ]);
        }

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controller\CourseAdmin] Got data to update course $course from " . $this->request->getIPAddress()
            );
            $validation = [ "name" => "permit_empty|course_pattern|is_unique[course.name]|max_length[100]" ];
            $validation_errors = [
                "name" => [
                    "course_pattern" => "Illegal characters for course name",
                    "is_unique" => "Course with same name already exists within the database"
                ]
            ];

            $data = [];
            if (empty($this->request->getPost("name")) === false) {
                $data[ "name" ] = $this->request->getPost("name", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK | FILTER_FLAG_ENCODE_AMP);

                /**
                 * Since the data inserted in the database is the encoded data after it has been sanitized,
                 * that makes the usage of $this->validate() useless since the data it would be validating is non-sanitized
                 * That would cause is_unique to fail since even though the same data is present in the database and the request
                 * They differ as the request's data is not sanitized and not escaped and the DB data is sanitized and escaped.
                 * This walkaround for that problem
                 */
                $validator = Services::validation();
                $validator->setRules($validation, $validation_errors);
                if ($validator->run($data) === false) {
                    log_message(
                        "alert",
                        "[App\Controller\CourseAdmin] Got invalid data for course $course from " . $this->request->getIPAddress()
                    );

                    $this->session->setFlashdata("validation", $validator->getErrors());
                    return $this->redirect((string) current_url(true));
                }

                $this->db->update($course, $data);
                log_message(
                    "debug",
                    "[App\Controller\SubjectAdmin] Updated subject data $course with data from " . $this->request->getIPAddress()
                );
            }

            return $this->redirect(route_to("admin_course_dashboard"));
        }
    }
}