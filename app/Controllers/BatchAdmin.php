<?php
namespace App\Controllers;

class BatchAdmin extends BaseController {
    public function __construct() {
        $this->db = model("App\Models\Batch");
        $this->session_key = "loggedIn";
    }

    public function index() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("admin_index"));

        $course = $this->request->getGet("course", FILTER_SANITIZE_STRING);
        $batches = $this->db->select("batch.id AS id, batch.course AS course_id, course.name AS course_name")
                            ->join("course", "course.id = batch.course", "INNER");

        if (is_null($course) === false) $batches = $batches->where("batch.course", $course);
        $batches = $batches->findAll();

        return view("admin/dashboard_batch", [
            "batches" => $batches
        ]);
    }

    public function batchAdd() {
        if ($this->LoggedIn() === false)     return $this->redirect(route_to("admin_index"));

        // When called via the HTTP POST method, validate the data and either add to database or store the validation errors and redirect
        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controllers\BatchAdmin] Got batch data from /admin/add-batch from " . $this->request->getIPAddress()
            );

            if ($this->validate("batch") === false) {
                // Alert the admin about possibly corrupt and malicious data being inserted into the database
                log_message(
                    "alert",
                    "[App\Controller\BatchAdmin] Got invalid batch data sent from " . $this->request->getIPAddress()
                );

                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect((string) current_url(true));
            }

            // Data has been validated against the rules. Now create the data array for QueryBuilder to insert into database.
            $batch_data = [
                "id" => $this->request->getPost("id", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP),
                "course" => $this->request->getPost("course")
            ];
            $this->db->insert($batch_data);

            // Now that a new teacher data has been added, log the success message in the logs for the admin with the new teacher's ID.
            // And then redirect to the teacher listings for the admin
            log_message(
                "alert",
                "[App\Controllers\BatchAdmin] {$batch_data["id"]} has been successfully added by " . $this->request->getIPAddress()
            );

            $this->session->setFlashdata("batch", $batch_data["id"]);
            $this->session->setFlashdata("redirect", route_to("admin_batch_dashboard"));

            return $this->redirect(route_to("admin_routine_populate"));
        }

        log_message(
            "debug",
            "[App\Controllers\BatchAdmin] Trying to add a new batch into the database. Triggered by " . $this->request->getIPAddress()
        );

        // When called using HTTP GET method, get all the validation errors from the session's flash data and render the form
        $validation = $this->session->getFlashData("validation");
        return view("admin/add_batch", [
            "validation" => $validation
        ]);
    }
}