<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;
use Config\Services;

class BaseController extends Controller {

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
    protected array $helpers = [ "date", "text", "url", "security", "filesystem" ];
	protected ?\CodeIgniter\Session\Session $session = null;
	protected ?\CodeIgniter\Model $db = null;
	protected ?string $session_key = null;
	protected ?\CodeIgniter\Security\Security $security = null;
	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger) {
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		$this->session = session();
		$this->security = Services::security();
	}

	protected function redirect(string $path) {
		// A helper class that can be used to issue redirects to given paths
		return $this->response->setStatusCode(302)->setHeader("Location", $path);
	}

	protected function saveFile(string $form_field): ?string {
	    $file_db = model("App\Models\File");
	    $file = $this->request->getFile($form_field);
	    if (($file->isValid() === false) && ($file->getError() ===4)) return null;
	    $length = random_int(10, 100);

	    if (function_exists("openssl_random_pseudo_bytes")) {
	        $strong = true;
	        $name = bin2hex(openssl_random_pseudo_bytes($length, $strong));
        } else
	        $name = bin2hex(random_bytes($length));

	    $file_data = [
            "mime" => $file->getMimeType(),
	        "path" => UPLOADPATH . $file->store(),
            "name" => $name
        ];
	    $file_db->insert($file_data);

	    return $name;
    }

    protected function LoggedIn(): bool {
	    $value = $this->session->get($this->session_key);
	    if (empty($value)) {
	        $currentUrl = current_url(true);
	        if (is_null($currentUrl))   $currentUrl = "< UNKNOWN URL >";

	        log_message(
	            "alert",
                "[App\Controller\BaseController] Blocked un-authenticated request to {$currentUrl->getPath()} from {$this->request->getIPAddress()} using {$this->request->getMethod(true)}"
            );

	        return false;
        }

	    return true;
    }
}
