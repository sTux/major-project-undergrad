<?php
namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Database\BaseBuilder;
use CodeIgniter\I18n\Time;
use Config\Database;


class student extends BaseController {
    use ResponseTrait;
    public function __construct() {
        // Load the the student model for the entire class to use
        $this->db = model("App\Models\Student");
        $this->session_key = "student";
    }

    public function index() {
        // Check the session data to see if the session represents a logged in student. 
        // If it is, then redirect to the student's dashboard
        if ($this->LoggedIn())     return $this->redirect(route_to("student_dashboard"));

        // Check to see if we're been redirected from a failed login attempt.
        // If that's the case, check for flash data and pass it to the view
        $error = $this->session->getFlashdata("error");
        if ($error === null)  $error = "";

        return view('student/login', [
            "errorMSG" => $error
        ]);
    }

    public function login() {
        // For safety re-initialize the entire session data
        $this->session->stop();
        log_message("debug", "[App\Controllers\Student] Recreated a new session for login {$this->request->getIPAddress()}");


        // This method only handles the HTTP GET request and other methods will redirect to student dashboard
        if ($this->request->getMethod(true) !== "POST") {
            log_message("debug", "[App\Controllers\Student] Invalid method {$this->request->getMethod(true)} for /student/login");
            return $this->redirect(route_to("student_index"));
        }

        $studentID = $this->request->getPost("username");
        $password = $this->request->getPost("password");

        // Use the QueryBuilder to build queries and get the student record from the database.
        // This returns an array of objects containing the records that marches the student ID. SHOULD ALWAYS BE 1 RECORD PER QUERY
        $student = $this->db->getWhere([ "id" => $studentID ])->getRow();

        // A basic check to see if the input student ID is valid ID or not.
        // If not then redirect to the login form with the appropriate error message
        if (empty($student)) {
            $this->session->setFlashdata("error", "Wrong Student ID");
            return $this->redirect(route_to("student_index"));
        }

        // If password verification fails, then set the error message and redirect to login page
        if (password_verify($password, $student->password) === false) {
            $this->session->setFlashdata("error", "Wrong password");
            return $this->redirect(route_to("student_index"));
        }

        // Since students can be marked as deleted rather than being removed from database, a login attempt to such a account must be disallowed
        if ($student->left_at !== null) {
            $this->session->setFlashdata("error", "Account disabled");
            return $this->redirect(route_to("student_index"));
        }


        // Since the passwords are set by the admin during the student record creation,
        // Allow the students to create their own password during the first login of their account
        if ($password === "student") {
            $this->session->set("id", $studentID);
            return $this->redirect(route_to("student_set_password"));
        }

        // Mark in the session data that the student has logged in properly
        $this->session->set($this->session_key, $student->id);
        log_message("debug", "[App\Controller\Student] {$student->id} successfully logged in with session " . session_id());

        return $this->redirect(route_to("student_dashboard"));
    }

    public function dashboard() {
        // Check and validate a active student session is available and sent to the app
        $student = $this->session->get("student");
        if ($this->LoggedIn() === false)  return $this->redirect(route_to("student_index"));

        // Now that we know the session is a valid student login, use this data to fetch their details
        $student = $this->db->getWhere([
            "id" => $student
        ])->getRow();

        if (empty($student))
            return $this->response->setStatusCode(404);

        // Render the view with the details of the student
        return view('student/dashboard', (array) $student);
    }

    public function pictureAdd() {
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "POST")  return $this->failForbidden();
        $picture = $this->saveFile("picture");
        $student = $this->session->get($this->session_key);
        $this->db->update($student, [ "picture" => $picture ]);
        log_message(
            "debug",
            "[App\Controllers\Student] Successfully changed picture for student " . $this->session->get($this->session_key)
        );

        return $this->redirect(route_to("student_index"));
    }

    public function setPassword() {
        // A check to see if the student tried a login before being redirected to /student/set-password.
        // Allows us to not require to send student ID using the URL because the state is saved in the SERVER session files
        $studentID = $this->session->get("id");
        if (empty($studentID)) {
            log_message(
                "alert",
                "[App\Controllers\Student] /student/set-password was called without any student id session data. Triggered by " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("student_index"));
        }

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controllers\Student] Trying to set password for $studentID. Triggered By " . $this->request->getIPAddress()
            );

            if ($this->validate("password") === false) {
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect(route_to("student_set_password"));
            }

            $password = $this->request->getPost("password");
            $this->db->update($studentID, [
                "password" => $password
            ]);

            $this->session->remove($this->session_key);
            $this->session->remove("id");
            log_message(
                "alert",
                "[App\Controllers\Student] Student $studentID password updated. Triggered By " . $this->request->getIPAddress()
            );
            return $this->redirect(route_to("student_index"));
        }
        // When the /student/set-password is called via GET method, then log the message as alert
        // Check for validation errors for any previous POST submissions and render the view
        log_message(
            "debug",
            "[App\Controllers\Student] /student/set-password called for student $studentID"
        );

        $validation = $this->session->getFlashdata("validation");

        return view("create_password", [
            "allowUserName" => true,
            "validation" => $validation
        ]);
    }

    public function notices() {
        if ($this->LoggedIn() === false)  return $this->redirect("/student");
        // Check and validate a active student session is available and sent to the app
        $student = $this->session->get("student");
        
        $student_batch = $this->db->where("id", $student)->get()->getRow();
        $student_batch = $student_batch->batch;
        $database = model("App\Models\Notice");
        $notices = $database->where("for_batch", $student_batch)->findAll();

        return view("student/notice_list", [
            "notices" => $notices
        ]);
    }

    public function notice() {
        if ($this->LoggedIn() === false)  return $this->redirect(route_to("student_index"));
        // Check and validate a active student session is available and sent to the app
        $student = $this->session->get("student");
        
        $batch = $this->db->where("id", $student)->get()->getRow()->batch;
        $number = $this->request->getGet("number", FILTER_SANITIZE_STRING);
        $notice_db = model("App\Models\Notice");
        $notice = $notice_db->select("title, text, issued, issued_by, file")
                            ->where("for_batch", $batch)->where("id", $number)
                            ->first();
        $teacher_db = model("App\Models\Teacher");
        $teacher = $teacher_db->select("name")->where("id", $notice->issued_by)->first();

        $data = [
            "title" => $notice->title,
            "text" => $notice->text,
            "issued" => Time::parse($notice->issued, "Asia/Kolkata")->humanize(),
            "issued_by" => $teacher->name,
            "batch" => $batch,
            "file" => $notice->file
        ];

        return view("student/notice", $data);
    }

    public function logout() {
        // Clear the session data and redirect to login page
        $this->session->destroy();
        return $this->redirect(route_to("index"));
    }

    public function changePassword() {
        if ($this->LoggedIn() === false)    return $this->redirect(route_to("student_index"));
        $student = $this->session->get($this->session_key);

        if ($this->request->getMethod(true) === "POST") {
            log_message(
                "debug",
                "[App\Controller\Student] Trying to change $student password. Triggered by ". $this->request->getIPAddress()
            );

            /**
             * Since we are updating the current password, so first we need to make sure that the student knows the password
             * After that, we need to update the record for the new password
             */
            $current_password = $this->request->getPost("currpassword");
            $password_record = $this->db->find($student);
            if (password_verify($current_password, $password_record->password) === false) {
                $this->session->setFlashdata("validation", [ "Wrong password" ]);
                return $this->redirect(route_to("student_change_password"));
            }

            if ($this->validate("password") === false) {
                $this->session->setFlashdata("validation", $this->validator->getErrors());
                return $this->redirect(route_to("student_change_password"));
            }
            $password = $this->request->getPost("password");
            $this->db->update($student, [ "password" => $password ]);

            return $this->redirect(route_to("student_logout"));
        }

        log_message(
            "debug",
            "[App\Controller\Student] Password change for $student requested by " . $this->request->getIPAddress()
        );
        $validation = $this->session->getFlashdata("validation");

        return view("change_password", [ "validation" => $validation ]);
    }

    public function getClasses() {
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "GET")   return $this->failForbidden();

        $class_db = Database::connect()->table("class");
        $week = date("W");
        $class = $class_db->select("id, topic, taught_by")
                          ->whereIn("routine_id", function (BaseBuilder $builder) {
                              return $builder->select("id")->from("class_routine")
                                      ->where("routine_id", function (BaseBuilder $inner) {
                                          $batch = $this->db->select("batch")->where("id", $this->session->get($this->session_key))->first();
                                          $day = date("l");
                                          return $inner->select("id")->from("routine")
                                                 ->where("batch", $batch->batch)->where("day", $day);
                                      });
                          })->where("week", $week)
                          ->get()->getResult();

        return $this->response->setJSON($class);
    }

    public function getClass() {
        if ($this->LoggedIn() === false || $this->request->getMethod(true) !== "GET")    return $this->failForbidden();

        $class_id = $this->request->getGet("class");
        $class_db = Database::connect()->table("class");

        $query = $class_db->select("class.id AS class, class.topic AS topic, teacher.name AS teacher, class_resources.resource AS resource")
                          ->join("teacher", "class.taught_by = teacher.id")
                          ->join("class_resources", "class_resources.for_class = class.id")
                          ->where("class.id", $class_id)->get(1)->getRow(1, "array");

        return $this->response->setJSON($query);
    }

    public function routine() {
        if ($this->LoggedIn() === false)   $this->redirect(route_to("student_index"));

        return view("student/routine");
    }

    public function attendanceAdd() {
        /**
         * Mapped to /student/api/attend, This method is called via XHR POST and is used to add attendance record
         * for a student after lecture video is finished.
         * This method receives the id of the class for which attendance is to be recorded and inserts the student attendance record
         */
        if ($this->LoggedIn() === false || $this->request->isAJAX() === false || $this->request->getMethod(true) !== "POST")    return $this->failForbidden();

        $attendance_table = Database::connect()->table("attendance");
        $records_db = Database::connect()->table("attendance_record");
        $class_id = $this->request->getPost("class_id");

        $class_id = $attendance_table->select("id")->where("class_id", $class_id)->get(1)->getRow(1);
        $id = $class_id->id;
        $joined = date("c");
        $student = $this->session->get($this->session_key);
        $record_data = [
            "day" => $id,
            "joined" => $joined,
            "student" => $student
        ];
        $records_db->insert($record_data);

        return $this->respondCreated("Attendance recorded");
    }

    public function fetchNotes() {
        /**
         * Mapped to /student/api/notes, This method is used to fetch notes for a particular class and deliver it to the students
         */
        if ($this->LoggedIn() === false || $this->request->isAJAX() === false)  return $this->failForbidden();

        $resources_table = Database::connect()->table("class_resources");
        $class = $this->request->getGet("class", FILTER_SANITIZE_NUMBER_INT);

        $notes = $resources_table->select("name, resource")
                                 ->where("type", "note")
                                 ->where("for_class", $class)
                                 ->get()->getRow();

        return $this->response->setJSON($notes);
    }
}
