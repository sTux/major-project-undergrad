<?php
namespace app\Models;
use CodeIgniter\Model;


class BaseModel extends Model {
    protected $DBGroup = "default";
    protected $returnType = "object";
    protected $beforeInsert = array("hashPassword");
    protected $beforeUpdate = array("hashPassword");

    protected function hashPassword(array $data) {
        /**
         * Hash the password recieved from the user. Beyond this point everythingworks with only the password's hash and the plaintext password is removed
        **/
        if (!isset($data['data']['password'])) return $data;

        $data["data"]["password"] = password_hash(
            $data['data']["password"], PASSWORD_DEFAULT, [ "cost" => 10 ]
        );

        return $data;
    }
}
