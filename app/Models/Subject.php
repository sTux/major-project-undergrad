<?php
namespace App\Models;


class Subject extends BaseModel {
    protected $table = "subject";
    protected $primaryKey = "code";

    protected $allowedFields = [
        "code",
        "taught_by",
        "name",
        "part_of"
    ];
}