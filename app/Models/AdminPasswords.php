<?php
namespace App\Models;
use CodeIgniter\Model;


class AdminPasswords extends BaseModel {
    /*
     * Model for the DB table `admin_passwords`. It fetches the latest password from the database and checks against the user's input.
     */

    // Connect to the default database to fetch the admin password
    protected $table = "admin_passwords";

    protected $primaryKey = "created_at";
    protected $createdField = "created_at";
    protected $allowedFields = ["password"];
}
