<?php
namespace App\Models;
use CodeIgniter\Model;


class Teacher extends BaseModel {
    protected $table = "teacher";
    protected $primaryKey = "id";
    protected $deletedField = "left_at";
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        "id",
        "password",
        "name",
        "picture",
    ];
}
