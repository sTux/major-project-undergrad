<?php
namespace App\Models;


class ClassRoutine extends BaseModel {
    protected $table = "class_routine";
    protected $primaryKey = "id";

    protected $allowedFields = [
        "subject_id",
        "routine_id",
        "teacher",
    ];
}