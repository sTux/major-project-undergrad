<?php
namespace App\Models;


class Batch extends BaseModel {
    protected $table = "batch";
    protected $primaryKey = "id";

    protected $allowedFields = [
        "id",
        "course"
    ];
}
