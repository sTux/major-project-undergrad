<?php
namespace App\Models;

class File extends BaseModel {
    protected $table = "uploaded_file";
    protected $primaryKey = "name";
    protected $createdField = "created";

    protected $allowedFields = [
        "name",
        "path",
        "mime"
    ];
}