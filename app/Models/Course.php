<?php
namespace App\Models;


class Course extends BaseModel {
    protected $table = "course";
    protected $primaryKey = "id";

    protected $allowedFields = [
        "id",
        "name"
    ];
}
