<?php
namespace App\Models;

class Notice extends \App\Models\BaseModel {
    protected $table = "notices";
    protected $primaryKey = "id";

    protected $allowedFields = [
        "title",
        "text",
        "file",
        "issued_by",
        "for_batch"
    ];
    protected $createdField = "issued";
}