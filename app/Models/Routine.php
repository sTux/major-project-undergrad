<?php
namespace App\Models;


class Routine extends BaseModel {
    protected $table = "routine";
    protected $primaryKey = "id";

    protected $allowedFields = [
        "batch",
        "day"
    ];
}