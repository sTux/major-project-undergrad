<?php
namespace App\Models;


class Student extends BaseModel {
    protected $table = "student";
    protected $primaryKey = "id";
    protected $deletedField = "left_at";
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        "id",
        "password",
        "name",
        "picture",
        "batch"
    ];
}
