<?php
$cssFile = base_url("/static/css/welcome_message.css");
$jsFile = base_url("/static/js/welcome_message.js");
?>
<!DOCTYPE html>
<html lang="en">
<head>
     <title> Welcome to VirtualClass -- Remote education system </title>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- LOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">Virtual Education</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="#"> Welcome To Virtual Education System </a></li>
                         <li><a href="#"> Call @ +91-1234567890 </a></li>
                         <li><a href="mailto:admin@school.com"> Mail @ admin@school.com </a></li>
                    </ul>
               </div>

          </div>
     </section>

     <section id="feature">
          <div class="container">
               <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-offset-4">
                         <div class="feature-thumb">
                              <ul class="social1-icon">
                                    <li><a class="fa fa-lock" attr="login icon"></a></li>
                              </ul>
                              <h3>Login</h3>
                              <button type="button" class="btn btn-primary btn-block"><a href="/student">Login For Student</a></button>
                              <button type="button" class="btn btn-primary btn-block"><a href="/teacher">Login For Teacher</a></button>
                         </div>
                    </div>
               </div>
          </div>
     </section>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $cssFile ?>">

    <!-- SCRIPTS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?= $jsFile ?>"></script>
</body>
</html>
