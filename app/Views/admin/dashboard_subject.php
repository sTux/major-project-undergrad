<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
    <div class="container">
        <div class="row">
	        <a href="<?= route_to("admin_subject_add") ?>"><button class="btn btn-primary"> Add Subject </button></a>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center" colspan="6">
                        <strong>
                            <h2> Subject Info </h2>
                        </strong>
                    </th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th> Subject Code </th>
                    <th> Subject Name </th>
                    <th> Under Course </th>
                    <th> Taught By </th>
                    <th> Action </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <?php foreach ($subjects as $subject) { ?>
                    <td> <?= $subject->code ?></td>
                    <td> <?= $subject->name ?></td>
	                <td><a href="?course=<?= $subject->course_code ?>"><?= entities_to_ascii($subject->course_name) ?></a></td>
	                <td><a href="?teacher=<?= $subject->teacher_id ?>"><?= $subject->teacher_name ?></a></td>
                    <td>
                        <ul class="social-icon">
<!--                            <li><a href="#" class="fa fa-trash" title="Delete" data-toggle="popover" data-trigger="hover"></a></li> &nbsp &nbsp-->
                            &nbsp <li><a href="<?= route_to("admin_subject_edit") ?>?subject=<?= $subject->code ?>" class="fa fa-pencil" title="Edit" data-toggle="popover" data-trigger="hover"></a></li>
                        </ul>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>

</section>
<?= $this->endSection() ?>