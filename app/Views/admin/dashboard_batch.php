<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
  <div class="container">
    <div class="row">
      <a href="<?= route_to("admin_batch_add") ?>"><button class="btn btn-primary"> Add Batch </button></a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center" colspan="6">
              <strong>
                <h2>Batch Info</h2>
              </strong>
            </th>
          </tr>
        </thead>
	      <thead>
          <tr>
            <th> Batch ID </th>
            <th> Course Name </th>
          </tr>
          </thead>
        <tbody>
          <tr>
            <?php foreach ($batches as $batch) { ?>
              <td> <?= $batch->id ?></td>
              <td><a href="?course=<?= $batch->course_id ?>"> <?= entities_to_ascii($batch->course_name) ?> </a></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>

</section>
<?= $this->endSection() ?>