<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
    <div class="container">
        <div class="row">
            <?= view("admin/error_message", [ "validation" => $validation ]) ?>
	        <div class="col-md-4 col-sm-4 col-lg-offset-4">
                <div class="feature-thumb">
                    <div class="admin-icon fa fa-user"></div>
                    <h3> Batch </h3>
                    <form action="<?= (string) current_url(true) ?>" method="POST">
                        <?= csrf_field() ?>
                        <div class="form-group">
                            <label for="batchID"> Batch ID: </label>
                            <input type="text" name="id" class="form-control" placeholder="Enter Batch ID" id="batchID">
                        </div>
                        <div class="form-group">
                            <label for="course"> Course Name: </label>
                            <select class="form-control" id="course" name="course"></select>
                        </div>
                        <button type="submit" class="btn btn-primary"> Add Batch </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
<script type="text/javascript">
    $(document).ready(function() {
        $.get("/api/courses", function(data) {
            $("#course").html(data);
        })
    });
</script>    
<?= $this->endSection() ?>

