<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<!-- FEATURE -->
<section id="feature">
  <div class="container">
    <div class="row">
	  <a href="<?= route_to("admin_course_add") ?>"><button class="btn btn-primary"> Add Course </button></a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center" colspan="6">
              <strong>
                <h2>Course Info</h2>
              </strong>
            </th>
          </tr>
        </thead>
        <thead>
          <tr>
            <th> Course ID </th>
            <th> Course Name </th>
            <th> Action </th>
          </tr>
          </thead>
        <tbody>
          <?php foreach ($courses as $course) { ?>
            <tr>
              <td> <?= $course->id ?> </td>
              <td> <?= $course->name ?> </td>
              <td>
                <ul class="social-icon">
<!--	               <li><a href="#" class="fa fa-trash" title="Delete" data-toggle="popover" data-trigger="hover"></a></li> &nbsp &nbsp-->
	               &nbsp;<li><a href="<?= route_to("admin_course_edit") ?>?course=<?= $course->id ?>" class="fa fa-pencil" title="Edit" data-toggle="popover" data-trigger="hover"></a></li>
                </ul>

              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
</section>
<?= $this->endSection() ?>
