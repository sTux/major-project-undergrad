<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>

<section id="feature">
	<div class="container">
		<div class="row">
			<form class="form-inline">
				<div class="form-group">
					<label for="day"> Select Day: </label>
					<select class="form-control" id="day" onchange="routine_data.day = this.value; getRoutine(); $('#add-class').prop('disabled', true)">
						<option selected disabled> --- Select A Day --- </option>
						<option value="Monday"> Monday </option>
						<option value="Tuesday"> Tuesday </option>
						<option value="Wednesday"> Wednesday </option>
						<option value="Thursday"> Thursday </option>
						<option value="Friday"> Friday </option>
						<option value="Saturday"> Saturday </option>
						<option value="Sunday"> Sunday </option>
					</select>
				</div>
				<div class="form-group">
					<label for="course"> Select Course: </label>
					<select class="form-control" id="course" onchange="getBatch(this.value); $('#dynamic_form').hide()"></select>
				</div>&nbsp;&nbsp;&nbsp;
				<div class="form-group">
					<label for="batch"> Select Batch: </label>
					<select class="form-control" id="batch" onchange="routine_data.batch = this.value; getRoutine();"></select>
				</div>
			</form>
		</div>
	</div>

	<table class="table table-bordered">
			<thead>
			<tr id="title"></tr>
			</thead>
			<thead>
				<tr>
					<th> Routine Number </th>
					<th> Subject </th>
					<th> Teacher </th>
					<th> For Batch </th>
					<th> Action </th>
				</tr>
			</thead>
		<tbody id="table-data">
		</tbody>
	</table>
	<button type="button" class="btn btn-success" id="add-class" onclick="$('#dynamic_form').toggle()" disabled> Add Classes </button>

	<div class="container" id="dynamic_form" hidden>
		<?= $this->include("admin/add_routine") ?>
	</div>

	<div class="container" id="dynamic_edit" hidden>
		<?= $this->include("admin/modify_routine") ?>
	</div>
</section>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
<script>
	let routine_data = {};

	$(document).ready(function() {
        $.get("/api/courses/", function (data) { $("#course").html(data) });
    })

	function getBatch(course) {
	    $.get(`/api/batch/${course}`, function (data) {
	        $("#batch").html(data)
            $("#add-class").prop("disabled", false)
	    });
	    $("#title").html(`Routine for ${course}`)
    }

    function updateForm(routine_id) {
	    $("#routine-id").val(routine_id);
	    $("#dynamic_edit").toggle()
    }

    function getRoutine() {
        $("#table-data").html("")

	    $.get(`<?= route_to("admin_api_routine") ?>`, routine_data, function (data) {
            for (const routine_class of data) {
                let html = `<tr>
	                        <td>${routine_class["id"]}</td>
	                        <td>${routine_class.subject}</td>
	                        <td>${routine_class.teacher}</td>
							<td>${routine_class.batch}</td>
							<td>
								<ul class="social-icon">
									<li><a class="fa fa-edit" onclick="updateForm(${routine_class['id']})"></a></li>
								</ul>
							</td>
						</tr>`;
                $("#table-data").append(html)
            }
        })
    }

    $("#dynamic_field").submit(function (event) {
        event.preventDefault();
        ajaxSubmit(this)
	    getRoutine();
    });

    $("#dynamic_update").submit(function (event) {
        event.preventDefault();
        ajaxUpdate(this)
        getRoutine();
    });
</script>
<?= $this->endSection() ?>
