<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
  <div class="container">
    <div class="row">
        <?= view("admin/error_message", [ "validation" => $validation ]) ?>
	    <div class="col-md-4 col-sm-4 col-lg-offset-4">
        <div class="feature-thumb">
	      <div class="social-icon fa fa-user"></div>
          <h3> Add New Teacher </h3>
          <form action="<?= (string) current_url(true) ?>" method="POST">
            <?= csrf_field() ?>
            <div class="form-group">
              <label for="id"> Teacher ID: </label>
              <input type="text" class="form-control" placeholder="Enter Teacher ID" id="id" name="id" required>
            </div>

            <div class="form-group">
              <label for="name"> Teacher Name: </label>
              <input class="form-control" placeholder="Enter teacher name" id="name" name="name" required>
            </div>

            <button type="submit" class="btn btn-primary"> Add Teacher </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>
