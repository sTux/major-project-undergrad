<?php if (empty($validation) === false) { ?>
<div class="container">
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <? foreach ($validation as $error) { ?>
		  <strong><?= $error ?></strong> <br/>
	  <? } ?>
  </div>
</div>
<?php } ?>
