<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
  <div class="container">
    <div class="row">
	  <a href="<?= route_to("admin_teacher_add") ?>"><button class="btn btn-primary"> Add Teacher </button></a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center" colspan="6">
              <strong>
                <h2>Teacher Info</h2>
              </strong>
            </th>
          </tr>
        </thead>
        <thead>
          <tr>
            <th> Teacher ID </th>
            <th> Teacher Name </th>
            <th> Action </th>
          </tr>
          </thead>
        <tbody>
          <tr>
            <?php foreach ($teachers as $teacher) { ?>
              <td> <?= $teacher->id ?></td>
              <td> <?= $teacher->name ?></td>
              <td>
                <ul class="social-icon">
	                <li><form method="POST" action="<?= route_to("admin_teacher_delete") ?>">
                            <?= csrf_field() ?>
			                <input type="hidden" name="teacher" value="<?= $teacher->id ?>">
			                <a class="fa fa-trash" title="Delete" data-toggle="popover" data-trigger="hover" onclick="doDelete.call(this)"></a>
		                </form></li>&nbsp;&nbsp;&nbsp;
                  &nbsp <li><a href="<?= route_to("admin_teacher_edit") ?>?teacher=<?= $teacher->id ?>" class="fa fa-pencil" title="Edit" data-toggle="popover" data-trigger="hover"></a></li>
                </ul>
              </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>

</section>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
	<script>
        function doDelete(student) { if (confirm("Are you sure?") === true)  this.parentNode.submit() }
	</script>
<?= $this->endSection() ?>