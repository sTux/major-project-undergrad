<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
    <div class="container">
        <div class="row">
            <?= view("admin/error_message", [ "validation" => $validation ]) ?>
	        <div class="col-md-4 col-sm-4 col-lg-offset-4">
                <div class="feature-thumb">
                    <ul class="admin-icon">
                        <li><a href="" class="fa fa-user"></a></li>
                    </ul>
                    <h3> Course </h3>
                    <form action="<?= (string) current_url(true) ?>" method="POST">
                        <?= csrf_field() ?>
                        <div class="form-group">
                            <label for="courseID"> Course ID: </label>
                            <input type="text" name="id" class="form-control" placeholder="Enter Course ID" id="courseID">
                        </div>
                        <div class="form-group">
                            <label for="name"> Course Name: </label>
                            <input type="text" name="name" class="form-control" placeholder="Enter Course name" id="name">
                        </div>

                        <button type="submit" class="btn btn-primary"> Add Course </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>

