<form class="form-inline" id="dynamic_field">
    <?= csrf_field("csrf_token") ?>
	<div class="form-group">
		<label for="teacher"> Select Teacher: </label>
		<select class="form-control" name="teacher" id="teacher" onmousedown="getTeachers(this)"
		        onchange="window.teacher = this.value"></select>
	</div>&nbsp;&nbsp;&nbsp;
	<div class="form-group">
		<label for="subject"> Select Subject: </label>
		<select class="form-control" name="subject" id="subject" onmousedown="getSubjects(this)"></select>
	</div>&nbsp;&nbsp;&nbsp;
	<button type="submit" class="btn btn-primary" id="add">Add & Submit</button>
</form>
<script>
    function getSubjects(element) {
        $.get(`/api/subjects?teacher=${window.teacher}`, function (data) { element.innerHTML = data });
    }

    function getTeachers(element) {
        $.get("/admin/api/teachers", function (data) { element.innerHTML = data })
    }

    function ajaxSubmit(data) {
        let formData = new FormData(data);
        formData.append("batch", routine_data.batch);
        formData.append("day", routine_data.day);

        $.ajax({
	        url: "<?= route_to("admin_routine_edit") ?>",
	        method: "POST",
	        data: formData,
	        processData: false,
	        contentType: false,
	        success: function (data) { $("csrf_token").val(data) }
        });
    }
</script>