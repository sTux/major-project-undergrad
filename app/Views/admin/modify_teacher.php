<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-lg-offset-4">
				<div class="feature-thumb">
					<ul class="social-icon">
						<li><a href="" class="fa fa-user"></a></li>
					</ul>
					<h3> Modify Teacher Data </h3>
					<form action="<?= (string)current_url(true) ?>" method="post">
                        <?= csrf_field() ?>
						<div class="form-group">
							<label for="name"> Teacher Name: </label>
							<input class="form-control" placeholder="Enter Teacher Name" id="name" name="name">
						</div>
						<div class="form-group">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" name="password-empty"> Delete Password
							</label>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<?= $this->endSection() ?>