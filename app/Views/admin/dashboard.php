<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>

<section id="feature">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="feature-thumb">
                    <div class="admin-icon fa fa-user"></div>
                    <h3> Student </h3>
                    <a href="<?= route_to("admin_student_dashboard") ?>"><button type="button" class="btn btn-primary btn-block"> Show Student </button></a>
                    <a href="<?= route_to("admin_student_add") ?>"><button type="button" class="btn btn-primary btn-block"> Add Student </button></a>
                </div>
            </div>

	        <div class="col-md-4 col-sm-4">
		        <div class="feature-thumb">
			        <div class="admin-icon fa fa-users"></div>
			        <h3> Teachers </h3>
			        <a href="<?= route_to("admin_teacher_dashboard") ?>"><button type="button" class="btn btn-primary btn-block"> Show Teachers </button></a>
			        <a href="<?= route_to("admin_teacher_add") ?>"><button type="button" class="btn btn-primary btn-block"> Add Teachers </button></a>
		        </div>
	        </div>

	        <div class="col-md-4 col-sm-4">
                <div class="feature-thumb">
	                <div class="admin-icon fa fa-graduation-cap"></div>
                    <h3> Courses </h3>
                    <a href="<?= route_to("admin_course_dashboard") ?>"><button type="button" class="btn btn-primary btn-block"> Show Courses </button></a>
                    <a href="<?= route_to("admin_course_add") ?>"><button type="button" class="btn btn-primary btn-block"> Add Courses </button></a>

                </div>
            </div>

	        <div class="col-md-4 col-sm-4">
		        <div class="feature-thumb">
			        <div class="admin-icon fa fa-graduation-cap"></div>
			        <h3> Subjects </h3>
			        <a href="<?= route_to("admin_subject_dashboard") ?>"><button type="button" class="btn btn-primary btn-block"> Show Subjects </button></a>
			        <a href="<?= route_to("admin_subject_add") ?>"><button type="button" class="btn btn-primary btn-block"> Add Subject </button></a>
		        </div>
	        </div>

            <div class="col-md-4 col-sm-4">
                <div class="feature-thumb">
                    <div class="admin-icon fa fa-braille"></div>
                    <h3> Batches </h3>
                    <a href="<?= route_to("admin_batch_dashboard") ?>"><button type="button" class="btn btn-primary btn-block"> Show Batches </button></a>
                    <a href="<?= route_to("admin_batch_add") ?>"><button type="button" class="btn btn-primary btn-block"> Add a Batch </button></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="feature-thumb">
	                <div class="admin-icon fa fa-calendar"></div>
                    <h3> Routine </h3>
                    <a href="<?= route_to("admin_routine_dashboard") ?>"><button type="button" class="btn btn-primary btn-block"> Show Routine </button></a>
<!--                    <a href="--><?//= route_to("admin_routine_add") ?><!--"><button type="button" class="btn btn-primary btn-block"> Add Routine </button></a>-->
                </div>
            </div>

        </div>
    </div>
</section>
<?= $this->endSection() ?>