<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<?= view("admin/error_message", [ "validation" => $validation ]) ?>
<section id="feature">
	<div class="container">
		<div class="row">
            <div class="col-md-4 col-sm-4 col-lg-offset-4">
                <div class="feature-thumb">
                    <div class="admin-icon fa fa-user"></div>
                    <h3> Subject </h3>
                    <form action="<?= (string) current_url(true) ?>" method="POST">
                        <?= csrf_field() ?>
	                    <div class="form-group">
		                    <label for="subjectName"> Subject Name: </label>
		                    <input type="text" name="subjectName" class="form-control" placeholder="Enter Subject Name" id="subjectName">
	                    </div>

	                    <div class="form-group">
		                    <label for="teacher"> Teacher Name: </label>
		                    <select class="form-control" id="teacher" name="teacher"></select>
	                    </div>
                        <button type="submit" class="btn btn-primary"> Add Course </button>
                    </form>
                </div>
            </div>

		</div>
    </div>
</section>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
<script type="text/javascript">
    $(document).ready(function () {
        $.get("/admin/api/teachers", function (data) {
            $("#teacher").html(data)
        })
    });
</script>
<?= $this->endSection() ?>
