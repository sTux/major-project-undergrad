<!DOCTYPE html>
<html lang="en">
<head>
     <title>Admin Panel</title>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">
               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>
                    <!-- lOGO TEXT HERE -->
                    <a href="<?= route_to("admin_index") ?>" class="navbar-brand">Admin Pannel</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="<?= route_to("admin_student_dashboard") ?>" class="smoothScroll">Students</a></li>
                         <li><a href="<?= route_to("admin_teacher_dashboard") ?>" class="smoothScroll">Teachers</a></li>
                         <li><a href="<?= route_to("admin_course_dashboard") ?>" class="smoothScroll">Courses</a></li>
                         <li><a href="<?= route_to("admin_batch_dashboard") ?>" class="smoothScroll">Batches</a></li>
	                     <li><a href="<?= route_to("admin_routine_dashboard") ?>" class="smoothScroll">Routines</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
	                     <li><a href="<?= route_to("admin_change_password") ?>">Change Password</a></li>
                         <li><a href="<?= route_to("admin_logout")?>">Logout</a></li>
                    </ul>
               </div>
          </div>
     </section>
<?php
    // Put the custom JS and CSS loading before the renderSection so that over-riding these value can be made possible
    $css = base_url("/static/css/welcome_message.css");
    $js = base_url("/static/js/welcome_message.js");
?>
<?= $this->renderSection("content"); ?>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="<?= $css ?>">
     <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
     <script src="<?= $js ?>"></script>
<?= $this->renderSection("scripts") ?>
</body>
</html>
