<form class="form-inline" id="dynamic_update">
    <?= csrf_field("csrf_update") ?>
	<input type="hidden" id="routine-id" name="id" value="">
    <div class="form-group">
        <label for="teacher"> Select Teacher: </label>
        <select class="form-control" name="teacher" id="teacher" onmousedown="getTeachers(this)"
                onchange="window.teacher = this.value"></select>
    </div>&nbsp;&nbsp;&nbsp;
    <div class="form-group">
        <label for="subject"> Select Subject: </label>
        <select class="form-control" name="subject" id="subject" onmousedown="getSubjects(this)"></select>
    </div>&nbsp;&nbsp;&nbsp;
    <button type="submit" class="btn btn-primary" id="add"> Modify </button>
</form>
<script>
    function ajaxUpdate(data) {
        let formData = new FormData(data);

        $.ajax({
            url: "<?= route_to("admin_routine_update") ?>",
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
	        success: function (data) { $("#csrf_update").val(data) }
        });
    }
</script>