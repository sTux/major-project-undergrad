<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
    <div class="container">
        <div class="row">
            <?= view("admin/error_message", [ "validation" => $validation ]) ?>
	        <div class="col-md-4 col-sm-4 col-lg-offset-4">
                <div class="feature-thumb">
                    <ul class="admin-icon">
                        <li><a href="" class="fa fa-user"></a></li>
                    </ul>
                    <h3> Student </h3>
                    <form action="<?= (string) current_url(true) ?>" method="POST">
                        <?= csrf_field() ?>
                        <div class="form-group">
                            <label for="studentID"> Student ID: </label>
                            <input type="text" name="id" class="form-control" placeholder="Enter Student ID" id="studentID">
                        </div>
                        <div class="form-group">
                            <label for="name"> Student Name: </label>
                            <input type="text" name="name" class="form-control" placeholder="Enter student name" id="name">
                        </div>

                        <div class="form-group">
                            <label for="course"> Select Course: </label>
                            <select class="form-control" id="course" onchange="batches(this.value)"></select>
                        </div>

                        <div class="form-group">
                            <label for="batch"> Select Batch: </label>
                            <select class="form-control" id="batch" name="batch"></select>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
<script type="text/javascript">
    $(document).ready(function() {
        $.get("/api/courses", function(data) {
            $("#course").html(data);
        })
    });

    function batches(course) {
        $.get("/api/batch/" + course, function(data) {
            $("#batch").html(data);
        })
    }
</script>
<?= $this->endSection() ?>