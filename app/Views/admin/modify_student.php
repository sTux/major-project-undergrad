<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<section id="feature">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-lg-offset-4">
				<div class="feature-thumb">
					<ul class="social-icon">
						<li><a href="" class="fa fa-user"></a></li>
					</ul>
					<h3> Modify Student Data </h3>
					<form action="<?= (string)current_url(true) ?>" method="post">
                        <?= csrf_field() ?>
						<div class="form-group">
							<label for="name"> Name: </label>
							<input type="text" class="form-control" placeholder="Enter Name" id="name" name="name">
						</div>
						<div class="form-group">
							<label for="course"> Select Course: </label>
							<select class="form-control" id="course" onchange="batches(this.value)"></select>
						</div>
						<div class="form-group">
							<label for="batch"> Select Batch: </label>
							<select class="form-control" id="batch" name="batch"></select>
						</div>

						<div class="form-group">
							<label class="form-group">
								<input class="form-check-input" type="checkbox" name="password-empty"> Delete Password
							</label>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<? $this->endSection() ?>
<? $this->section("scripts") ?>
<script>
    $(document).ready(function() {
        $.get("/api/courses", function(data) {
            $("#course").html(data);
        })
    });

    function batches(course) {
        $.get("/api/batch/" + course, function(data) {
            $("#batch").html(data);
        })
    }
 </script>
<?= $this->endSection() ?>