<?= $this->extend("App\Views\admin\default") ?>
<?= $this->section("content") ?>
<!-- FEATURE -->
<section id="feature">
  <div class="container">
    <div class="row">
	  <a href="<?= route_to("admin_student_add") ?>"><button class="btn btn-primary"> Add Student </button></a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center" colspan="6">
              <strong>
                <h2>Student Info</h2>
              </strong>
            </th>
          </tr>
        </thead>
        <thead>
          <tr>
            <th> Student ID </th>
            <th> Student Name </th>
            <th> Batch </th>
            <th> Action </th>
          </tr>
          </thead>
        <tbody>
          <?php foreach ($students as $student) { ?>
            <tr>
              <td> <?= $student->id ?> </td>
              <td> <?= $student->name ?> </td>
              <td> <?= $student->batch ?></td>
              <td>
                <ul class="social-icon">
	                <li><form method="POST" action="<?= route_to("admin_student_delete") ?>">
                        <?= csrf_field() ?>
		                <input type="hidden" name="student" value="<?= $student->id ?>">
		                <a class="fa fa-trash" title="Delete" data-toggle="popover" data-trigger="hover" onclick="doDelete.call(this)"></a>
	                </form></li>&nbsp;&nbsp;&nbsp;
                    &nbsp;<li><a href="<?= route_to("admin_student_edit") ?>?student=<?= $student->id ?>" class="fa fa-pencil" title="Edit" data-toggle="popover" data-trigger="hover"></a></li>
                </ul>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
</section>
<?= $partial ?>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
<script>
    function doDelete(student) { if (confirm("Are you sure?") === true)  this.parentNode.submit() }
</script>
<?= $this->endSection() ?>
