<!DOCTYPE html>
<html>
<head>
    <title> Change Password </title>
</head>

<body>
<?php if (empty($validation) === false) { ?>
	<div class="container">
		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
            <? foreach ($validation as $error) { ?>
				<strong><?= $error ?></strong> <br/>
            <? } ?>
		</div>
	</div>
<?php } ?>
<div class="modal-dialog text-center">
        <div class="col-sm-9 main-section">
            <div class="modal-content">
                <div class="col-12 user-img">
                    <img src="<?= base_url("static/images/admin/face.png") ?>">
                </div>
                <div class="col-12 form-input">
                    <form action="<?= current_url() ?>" method="POST">
                        <?= csrf_field() ?>
                        <div class="form-group">
                            <input id="currpassword" name="currpassword" type="password" class="form-control" placeholder="Enter Password" required>
                        </div>
                        <div class="form-group">
                            <input id="password" name="password" type="password" class="form-control" placeholder="Enter New Password" required>
                            <span id="passcheck"> </span>
                        </div>
                        
                        <div class="form-group">
                            <input id="cpassword" name="cpassword" type="password" class="form-control" placeholder="Confirm Password" required disabled>
                            <span id="cpasscheck"> </span>
                        </div>
                        <button type="submit" id="submit" class="btn btn-success" disabled>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <link rel="stylesheet" type="text/css" href="<?= base_url("static/css/admin/login.css") ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
    <script src="<?= base_url("/static/js/forgetpassword.js") ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            let checker = function () {
                let match = $("#password").val() === $("#cpassword").val();
                if (match) {
                    $("#cpasscheck").html("Passwords match").css("color", "green")
                    $("#submit").prop("disabled", false);
                } else {
                    $("#cpasscheck").html("Passwords don't match").css("color", "red")
                    $("#submit").prop("disabled", true);
                }
            };
            let matcher = function () {
                let out = passwordStrength($("#password").val());
                $("#passcheck").html(out.html);
                if (out.enable) $("#cpassword").prop("disabled", false);
                else $("#cpassword").prop("disabled", true);
            };

            $('#password').on({
                keyup: matcher,
                paste: matcher
            });

            $('#cpassword').on({
                keyup: checker,
                paste: checker
            });
        });
    </script>
</body>

</html>