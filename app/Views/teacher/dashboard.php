<?= $this->extend("App\Views\\teacher\default") ?>
<?= $this->section("content") ?>
	<section id="team">
		<div class="container">
			<div class="row">

				<div class="col-md-12 col-sm-12">
					<div class="section-title">
						<h2>Teacher Information</h2>
					</div>
				</div>

				<div class="col-md-4 col-sm-5">
					<div class="team-thumb">
						<div class="team-image">
                            <?php if (!is_null($picture)) { ?>
								<img src="<?= route_to("media") ?>?name=<?= $picture ?>" class="img-responsive" alt="">
                            <?php } ?>
						</div>
						<div class="form-inline">
							<form enctype="multipart/form-data" method="POST" id="picture-form" action="<?= route_to("teacher_picture_set") ?>">
                                <?= csrf_field() ?>
								<div class="form-group">
									<label for="picture"> Select a new picture </label>
									<input type="file" id="picture" name="picture" accept="image/*" onchange="$('#picture-form').submit()">
								</div>
							</form>
						</div>
					</div>
					<div class="team-info">
						<h3> Name: <?= $name ?></h3>
						<span> Teacher ID: <?= $id ?> </span><br>
					</div>
				</div>
			</div>
		</div>
	</section>
<?= $this->endSection() ?>