<?= $this->extend("App\Views\\teacher\default") ?>
<?= $this->section("content") ?>
<section id="feature">
    <div class="container">
        <?php if (empty($validation) === false) { ?>
		    <div class="alert alert-danger alert-dismissible">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
                <? foreach ($validation as $error) { ?>
				    <strong><?= $error ?></strong> <br/>
                <? } ?>
		    </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-offset-4">
                <div class="feature-thumb">
	                <div class="admin-icon fa fa-user"></div>
                    <h3> Issue Notice </h3>
                    <form action="<?= (string) current_url(true) ?>" method="POST" enctype="multipart/form-data">
                        <?= csrf_field() ?>
	                    <?php
		                    $title = "";
		                    $body = "";
		                    $batch = "";
		                    if (isset($notice) && !is_null($notice)) {
                                $title = "value=\"$notice->title\"";
                                $body = "$notice->text";
                                $batch = "<option value='$notice->for_batch' selected>$notice->for_batch</option>";
		                    }
	                    ?>
                        <div class="form-group">
                            <label for="name"> Title: </label>
                            <input type="text" name="title" class="form-control" placeholder="Enter notice title" id="name" <?= $title ?>>
                        </div>

                        <div class="form-group">
                            <label for="course"> Select Course: </label>
                            <select class="form-control" id="course" onchange="batches(this.value)"></select>
                        </div>

                        <div class="form-group">
                            <label for="batch"> Select Batch: </label>
                            <select class="form-control" id="batch" name="batch"><?= $batch ?></select>
                        </div>

                        <div class="form-group">
                            <label for="comment"> Write Notice: </label>
                            <textarea class="form-control" rows="5" id="comment" name="text"> <?= $body ?> </textarea>
                        </div>

                        <div class="form-group">
                            <input type="file" class="form-control-file border" name="file" accept="application/pdf">
                        </div>

                        <button type="submit" class="btn btn-primary"> Create notice </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>

<?= $this->section("scripts") ?>
<script type="text/javascript">
    $(document).ready(function() {
        $.get("/api/courses", function(data) {
            $("#course").html(data);
        })
    });

    function batches(course) {
        $.get("/api/batch/" + course, function(data) {
            $("#batch").html(data);
        })
    }
</script>
<?= $this->endSection() ?>