<?= $this->extend("App\Views\\teacher\default") ?>
<? $this->section("content"); ?>
<section id="feature">
	<div class="container">
		<div class="row">
			<h3> Add Class </h3>
			<form action="<?= (string)current_url(true) ?>" method="POST" class="form-vertical" enctype="multipart/form-data">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="name"> Title: </label>
					<input type="text" name="title" class="form-control" id="name" required>
				</div>

				<div class="form-group">
					<label for="lecture"> Upload Class Video </label>
					<input type="file" id="lecture" name="lecture" accept="video/*" required>
				</div>

				<div class="form-group">
					<label for="note"> Notes: </label>
					<input type="file" id="note" name="note" accept="application/pdf">
				</div>
				<button type="submit" class="btn btn-primary"> Upload</button>
			</form>
		</div>
	</div>
</section>
<?= $this->endSection() ?>

