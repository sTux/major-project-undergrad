<?= $this->extend("App\Views\\teacher\default") ?>
<?= $this->section("content"); ?>
<section id="feature">
    <div class="container">
        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center" colspan="6">
                        <strong>
                            <h2> Notice List </h2>
                        </strong>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th> Notice Date </th>
                    <th> Notice Title </th>
                    <th> For Batch </th>
                    <th> Action </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php foreach ($notices as $notice) { ?>
                    <td> <?= (\CodeIgniter\I18n\Time::parse($notice->issued))->humanize() ?></td>
                    <td> <?= $notice->title ?> </td>
	                <td> <?= $notice->for_batch ?> </td>
                    <td>
                        <ul class="social-icon">
                            <li>
	                            <form method="POST" action="<?= route_to("teacher_notice_delete") ?>">
		                            <?= csrf_field() ?>
		                            <input type="hidden" name="notice" value="<?= $notice->id ?>">
	                                <a class="fa fa-trash" title="Delete" data-toggle="popover" data-trigger="hover" onclick="doDelete.call(this)"></a>
	                            </form>
                            </li>
                            &nbsp <li><a href="<?= route_to("teacher_notice_modify") ?>?notice=<?= $notice->id ?>" class="fa fa-pencil" title="Edit" data-toggle="popover" data-trigger="hover"></a></li>
                        </ul>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
</section>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
<script>
	function doDelete(notice) {
	    if (confirm("Are you sure?") === true)  this.parentNode.submit()
    }
</script>
<?= $this->endSection() ?>
