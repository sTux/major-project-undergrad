<?= $this->extend("App\Views\\teacher\default") ?>
<?= $this->section("content") ?>
	<!-- FEATURE -->
	<section id="feature">
		<div class="container">
			<div class="row">
				<table class="ttable table-striped table-responsive-lg">
					<thead>
					<tr>
						<th class="text-center">
							<strong>
								<h2> Student Attendance Info </h2>
							</strong>
						</th>
					</tr>
					</thead>
					<thead>
					<tr>
						<th> Student Name </th>
						<th> Class Joining Time </th>
					</tr>
					</thead>
					<tbody>
						<tr>
							<?php foreach ($records as $record) { ?>
								<td> <?= $record->name ?> </td>
								<td> <?= \CodeIgniter\I18n\Time::parse($record->joined)->humanize() ?> </td>
							<?php } ?>
						</tr>
					</tbody>
				</table>
	</section>
<?= $this->endSection() ?>