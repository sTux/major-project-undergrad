<?= $this->extend("App\Views\\teacher\default") ?>
<?= $this->section("content") ?>
	<!-- FEATURE -->
	<section id="feature">
		<div class="container">
			<div class="row">
				<form class="form-inline" id="dynamic_field">
					<div class="form-group">
						<div class="form-group">
							<label for="subject"> Select Subject: </label>
							<select class="form-control" id="subject" name="subject"></select>
						</div>&nbsp;&nbsp;&nbsp;

						<button type="submit" class="btn btn-primary" id="add">Show</button>
				</form>
			</div>
		</div>

		<table class="table table-striped table-responsive-lg">
			<thead>
			<tr>
				<th> Batch </th>
				<th> Day </th>
				<th> Action </th>
			</tr>
			</thead>
			<tbody id="table-body">
			</tbody>
		</table>
	</section>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
	<script>
		$(document).ready(function () {
		    $.get("/api/subjects", { teacher: "<?= $teacher ?>" },function (data) { $("#subject").html(data) })
        });

		$("#dynamic_field").submit(function (event) {
		    event.preventDefault();
		    $("#table-body").html("");

			$.get("<?= route_to("teacher_api_routine") ?>", { subject: $("#subject").val() }, function (data) {
                data.forEach(function (data) {
	                let element = `<tr>
										<td><h5>${data["batch"]}</h5></td>
										<td><h5>${data["day"]}</h5></td>
										<td>
											<ul class="social-icon">
												<li><a href="<?= route_to("teacher_class_add") ?>?routine=${data["id"]}" class="fa fa-plus"></a></li>
												<li><a href="<?= route_to("teacher_records") ?>?routine=${data["id"]}" class="fa fa-book"></a></li>
											</ul>
										</td>
									</tr>`;
	                $("#table-body").append(element)
                })
            })
        });
	</script>
<?= $this->endSection() ?>