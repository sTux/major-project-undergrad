<!DOCTYPE html>
<html lang="en">

<head>
     <title> Teacher -- Dashboard </title>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <a href="<?= route_to("teacher_dashboard") ?>" class="navbar-brand"> Home </a>
               </div>

               <div class="collapse navbar-collapse" id=".navbar-collapse">
                    <ul class="nav navbar-nav ml-auto text-center">
                         <li><a href="#top" class="smoothScroll"> About Me </a></li>
                         <li><a href="<?= route_to("teacher_routine") ?>">Routine</a></li>
	                     <li><a href="<?= route_to("teacher_notices") ?>"> View Notices </a></li>
                         <li><a href="<?= route_to("teacher_notify") ?>"> Add Notice </a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
	                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="<?= route_to("teacher_change_password") ?>">Change Password</a></li>
	                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="<?= route_to("teacher_logout") ?>">Logout</a></li>
                    </ul>
               </div>
          </div>
     </section>

     <?= $this->renderSection("content") ?>

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="<?= base_url("/static/css/welcome_message.css"); ?>">
     <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="<?= base_url("/static/js/welcome_message.js"); ?>"></script>

     <?= $this->renderSection("scripts") ?>
</body>
</html>