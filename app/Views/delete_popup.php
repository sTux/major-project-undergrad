<!-- The Modal -->
<div class="modal" id="delete-dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Are you sure?</h4>
        <button type="button" class="close" data-dismiss="modal">
          &times;
        </button>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <div class="form-group">
          <form action="<?= $delete_url ?>" method="POST">
            <?= csrf_field() ?>
            <button type="submit" class="btn btn-danger">Delete</button>
          <button type="button" class="btn btn-success" data-dismiss="modal"> Cancel </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
