<?= $this->extend("App\Views\student\default"); ?>
<?= $this->section("content"); ?>
	<nav class="col-md-2 d-none d-md-block bg-light sidebar">
		<div class="sidebar-sticky">
			<ul class="nav flex-column" id="sidebar-items">
				<li class="nav-item">
					<a class="nav-link active">
						<span data-feather="home"></span> Dashboard
					</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row" id="class-section" hidden>
			<?= $this->include("student/play_class") ?>
		</div>
	</div>
<?= $this->endSection() ?>
<?= $this->section("scripts") ?>
	<script>
        $(document).ready(function () {
            $.get("<?= route_to("student_api_classes") ?>", function (data) {
                data.forEach(function (element) {
                    let html = `<li class="nav-item">
									<a class="nav-link" onclick="fetchClass(${element['id']})"> <span data-feather="file"></span> ${element["topic"]}</a>
								</li>`;
                    $("#sidebar-items").append(html);
                })
            })
        })

		function fetchClass(element) {
            $.get("<?= route_to("student_api_class") ?>", { class: element }, function (data) {
                $("#class-section").show();
                $("#class-id").html(data.class)
                $("#class-title").html(data.topic)
	            $("#class-teacher").html(data.teacher)
	            $("#class-lecture").attr("src", `<?= route_to("media") ?>?name=${data.resource}`)
            })
        }
	</script>
<?= $this->endSection() ?>