<div class="video-container" style="height: 650px;">
	<div class="col-sm-8">
		<h4 style="color: black;" id="class-title"></h4>
		<h5 style="color: black;" id="class-teacher"></h5>
		<video class="video" controls autoplay id="class-lecture" onended="record()" onplay="getNotes()">
			<div id="token"><?= csrf_hash() ?></div>
			<div id="class-id"></div>
		</video>
	</div>

	<div class="col-sm-2" style="height: 650px;" id="notes-section"></div>
	<div>
	</div>
	<link rel="stylesheet" href="<?= base_url("/static/css/student/video.css") ?>">
	<script>
		function record() {
			let data = {
		        <?= csrf_token() ?>: $("#token").html() ,
				class_id: $("#class-id").html()
			}
			$.post("<?= route_to("student_api_attend") ?>", data, function (data) { alert(data) })
        }

        function getNotes() {
            $.get("<?= route_to("student_api_notes") ?>", {class: $("#class-id").html()}, function (data) {
                let html = `<div><a href="<?= route_to('media') ?>?name=${data['resource']}">${data['name']}</a></div>`
	            $("#notes-section").html(html)
            })
        }
	</script>
</div>
