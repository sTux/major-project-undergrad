<?= $this->extend("App\Views\student\default") ?>
<?= $this->section("content") ?>

<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="section-title">
					<h2 class="text-center">Student Information</h2>
				</div>
			</div>

			<div class=" col-lg-offset-4 col-md-offset-4 col-sm-4">
				<center>
					<div class="team-thumb">
						<div class="team-image">
                            <?php if (!is_null($picture)) { ?>
								<img src="<?= route_to("media") ?>?name=<?= $picture ?>" class="img-responsive" alt="">
                            <?php } ?>
							<div class="form-inline">
								<form enctype="multipart/form-data" method="POST" id="picture-form" action="<?= route_to("student_picture_set") ?>">
									<?= csrf_field() ?>
									<div class="form-group">
										<label for="picture"> Select a new picture </label>
										<input type="file" id="picture" name="picture" accept="image/*" onchange="$('#picture-form').submit()">
									</div>
								</form>
							</div>
						</div>
						<div class="team-info text-right">
							<h5>Name: <?= $name ?></h5>
							<h5>Student ID: <?= $id ?></h5>
							<span><h5>Batch: <?= $batch ?></h5></span>
						</div>
					</div>
				</center>
			</div>
		</div>
</section>

<?= $this->endSection() ?>
