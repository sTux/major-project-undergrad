<?= $this->extend("App\Views\student\default") ?>
<?= $this->section("content") ?>
<br><h1 class="text-center"><?= $title ?></h1>
<h5 class="text-center">Issued by <?= $issued_by ?> at <?= $issued ?></h5>

<p class="text-center"> <?= $text ?> </p>
<button type="button" class="btn btn-success"><a href="<?= base_url("/media?name=$file") ?>"> Download file </a></button>

</div>
<?= $this->endSection() ?>
