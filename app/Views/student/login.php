<!DOCTYPE html>
<html>
    <head>
        <title>Student Login</title>
    </head>
<body>
    <?php if ($errorMSG !== "") { ?>
    <div class="container">
      <div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?= $errorMSG ?></strong>
      </div>
    </div>
    <?php } ?>

    <div class="modal-dialog text-center">
    <div class="col-sm-9 main-section">
      <div class="modal-content">

        <div class="col-12 user-img">
            <img src="<?= base_url("/static/images/student/face.png") ?>">
        </div>

        <div class="col-12 form-input">
          <form action="<?= route_to("student_login") ?>" method="POST">
            <?= csrf_field() ?>
            <div class="form-group">
              <input name="username" type="text" class="form-control" placeholder="Enter Student ID" required>
            </div>
            <div class="form-group">
              <input name="password" type="password" class="form-control" placeholder="Enter Password" required>
            </div>
            <br>
            <button type="submit" class="btn btn-success">Login</button>
          </form>
        </div>

        <div class="col-12 forgot">
          <a href="<?= route_to("forget_password") ?>">Forgot Password?</a>
        </div>

      </div>
    </div>
  </div>
</body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
<link rel="stylesheet" type="text/css" href="<?= base_url("/static/css/student/login.css") ?>">
</html>
