<?= $this->extend("App\Views\student\default") ?>
<?= $this->section("content") ?>
<!-- TEAM -->
<section id="team">
     <div class="container">
          <div class="row">
               <div class="section-title">
	               <h2> Notices </h2>
                   <?php foreach ($notices as $notice) { ?>
	               <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false">
		               <div class="toast-header">
			               <a href="<?= route_to("student_notice") ?>?number=<?= $notice->id ?>">
				               <strong class="mr-auto"><?= $notice->title ?></strong>
			               </a>
		               </div>
		               <div class="toast-body">
			               <small><?= \CodeIgniter\I18n\Time::parse($notice->issued, "Asia/Kolkata")->humanize() ?></small>
		               </div>
	               </div>
                   <br><br>
                   <? } ?>
               </div>
          </div>
     </div>
</section>
<?= $this->endSection() ?>