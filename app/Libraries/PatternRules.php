<?php
namespace App\Libraries;
use Config\Database;


class PatternRules {
    public function password_pattern(string $str): bool {
        return (bool) preg_match('/^((?=.*[a-zA-Z]+)(?=.*[0-9]+)(?=.*[~!#$%\&*\-_+=\^@]+)).*$/i', $str);
    }
    
    public function course_pattern(string $str = null): bool {
			return (bool) preg_match('/[a-z0-9\']+$/i', $str);
	}

    public function is_unique_sanitized(string $str = null, string $field, array $data): bool {
        /**
         * Based upon CodeIgniter's is_unique validation method. But modified to use PHP filter_var to sanitize the input
         */
        // Grab any data for exclusion of a single row.
        [$field, $ignoreField, $ignoreValue] = array_pad(explode(',', $field), 3, null);
        $str = filter_var($str, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_AMP);

        // Break the table and field apart
        sscanf($field, '%[^.].%[^.]', $table, $field);

        $db = Database::connect($data['DBGroup'] ?? null);

        $row = $db->table($table)->select('1')->where($field, $str)->limit(1);

        if (! empty($ignoreField) && ! empty($ignoreValue)) {
            $row = $row->where("{$ignoreField} !==", $ignoreValue);
        }

        return (bool) ($row->get()->getRow() === null);
    }
}