<?php
namespace App\Database\Migrations;

class AddAdminPasswords extends \CodeIgniter\Database\Migration {
    protected $DBGroup = "tests";

    public function up() {
        $this->forge->addField([
            "password" => [
                "type" => "VARCHAR",
                "constraint" => 500,
                "null" => false,
                "unique" => true
            ],
            "created_at datetime NOT NULL DEFAULT current_timestamp()",
            "deleted_at" => [
                "type" => "DATETIME",
                "null" => true,
            ]
        ]);
        $this->forge->addPrimaryKey("created_at");
        $this->forge->createTable("admin_passwords", true);
    }

    public function down() {
    }
}