<?php
namespace App\Database\Migrations;
use CodeIgniter\Database\Migration;

class AddCourse extends Migration {
    protected $DBGroup = "tests";

    public function up() {
        
        $this->forge->addField([
            "id" => [
                "type" => "VARCHAR",
                "constraint" => 15,
                "null" => false
            ],
            "name" => [
                "type" => "VARCHAR",
                "constraint" => 100,
                "null" => false
            ]
        ]);
        $this->forge->addPrimaryKey("id");
        $this->forge->addUniqueKey("name");

        $this->forge->createTable("course", true);
    }

    public function down() {
    }
}