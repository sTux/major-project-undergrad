<?php
namespace App\Database\Migrations;
use CodeIgniter\Database\Migration;

class AddBatch extends Migration {
	protected $DBGroup = "tests";
	public function up() {
		$this->forge->addField([
			"id" => [
				"type" => "VARCHAR",
				"constraint" => 100,
				"null" => false				
			],
			"course" => [
				"type" => "VARCHAR",
				"constraint" => 15,
				"null" => false
			]
		]);
		$this->forge->addPrimaryKey("id");
		$this->forge->addForeignKey(
			"course",
			"course",
			"id",
		);

		$this->forge->createTable("batch", true);
	}

	public function down() {
	}
}
