-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 13, 2020 at 10:06 AM
-- Server version: 10.4.11-MariaDB-log
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtclass`
--
CREATE DATABASE IF NOT EXISTS `virtclass` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `virtclass`;

-- --------------------------------------------------------

--
-- Table structure for table `admin_passwords`
--

CREATE TABLE IF NOT EXISTS `admin_passwords` (
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `password` varchar(500) NOT NULL,
  PRIMARY KEY (`created_at`),
  UNIQUE KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `admin_passwords`:
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `routine` bigint(20) NOT NULL,
  `week` int(11) NOT NULL,
  `due` datetime NOT NULL,
  `batch` varchar(100) NOT NULL,
  `teacher` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `routine` (`routine`),
  KEY `batch` (`batch`),
  KEY `teacher` (`teacher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `assignments`:
--   `batch`
--       `batch` -> `id`
--   `routine`
--       `routine` -> `id`
--   `teacher`
--       `teacher` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments_submissions`
--

CREATE TABLE IF NOT EXISTS `assignments_submissions` (
  `assignment` bigint(11) NOT NULL,
  `student` varchar(255) NOT NULL,
  `file` varchar(100) NOT NULL,
  `submitted_at` datetime NOT NULL DEFAULT current_timestamp(),
  KEY `assignment` (`assignment`),
  KEY `student` (`student`),
  KEY `file` (`file`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `assignments_submissions`:
--   `assignment`
--       `assignments` -> `id`
--   `student`
--       `student` -> `id`
--   `file`
--       `uploaded_file` -> `name`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `day` date NOT NULL DEFAULT current_timestamp(),
  `teacher` varchar(255) NOT NULL,
  `class_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher` (`teacher`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `attendance`:
--   `class_id`
--       `class` -> `id`
--   `teacher`
--       `teacher` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_record`
--

CREATE TABLE IF NOT EXISTS `attendance_record` (
  `day` bigint(20) NOT NULL,
  `student` varchar(255) NOT NULL,
  `joined` timestamp NOT NULL DEFAULT current_timestamp(),
  KEY `day` (`day`),
  KEY `student` (`student`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `attendance_record`:
--   `student`
--       `student` -> `id`
--   `day`
--       `attendance` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE IF NOT EXISTS `batch` (
  `id` varchar(100) NOT NULL,
  `course` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `course` (`course`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `batch`:
--   `course`
--       `course` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `topic` varchar(500) NOT NULL,
  `taught_by` varchar(255) NOT NULL,
  `week` int(11) NOT NULL,
  `routine_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `taught_by` (`taught_by`),
  KEY `routine_id` (`routine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `class`:
--   `taught_by`
--       `teacher` -> `id`
--   `routine_id`
--       `class_routine` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `class_resources`
--

CREATE TABLE IF NOT EXISTS `class_resources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` enum('lecture','note','worksheet') NOT NULL,
  `resource` varchar(100) NOT NULL,
  `for_class` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resource_2` (`resource`),
  KEY `for_class` (`for_class`),
  KEY `resource` (`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `class_resources`:
--   `for_class`
--       `class` -> `id`
--   `resource`
--       `uploaded_file` -> `name`
--

-- --------------------------------------------------------

--
-- Table structure for table `class_routine`
--

CREATE TABLE IF NOT EXISTS `class_routine` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject_id` varchar(20) NOT NULL,
  `routine_id` bigint(20) NOT NULL,
  `teacher` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subject_id` (`subject_id`),
  KEY `routine_id` (`routine_id`),
  KEY `teacher` (`teacher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `class_routine`:
--   `routine_id`
--       `routine` -> `id`
--   `subject_id`
--       `subject` -> `code`
--   `teacher`
--       `teacher` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` varchar(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `course`:
--

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE IF NOT EXISTS `notices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `text` text DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `issued` timestamp NOT NULL DEFAULT current_timestamp(),
  `issued_by` varchar(255) DEFAULT NULL,
  `for_batch` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `issued_by` (`issued_by`),
  KEY `for_batch` (`for_batch`),
  KEY `file` (`file`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `notices`:
--   `for_batch`
--       `batch` -> `id`
--   `issued_by`
--       `teacher` -> `id`
--   `file`
--       `uploaded_file` -> `name`
--

-- --------------------------------------------------------

--
-- Table structure for table `routine`
--

CREATE TABLE IF NOT EXISTS `routine` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `batch` varchar(100) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `for` (`batch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `routine`:
--   `batch`
--       `batch` -> `id`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `routine_student`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `routine_student` (
`routine_id` bigint(20)
,`id` bigint(20)
,`subject` varchar(255)
,`teacher` varchar(100)
,`batch` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` varchar(255) NOT NULL,
  `password` varchar(300) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `batch` varchar(100) NOT NULL,
  `left_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `batch` (`batch`),
  KEY `picture` (`picture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `student`:
--   `batch`
--       `batch` -> `id`
--   `picture`
--       `uploaded_file` -> `name`
--

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `code` varchar(20) NOT NULL,
  `taught_by` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `part_of` varchar(15) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `name` (`name`),
  KEY `taught_by` (`taught_by`),
  KEY `part_of` (`part_of`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `subject`:
--   `part_of`
--       `course` -> `id`
--   `taught_by`
--       `teacher` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `id` varchar(255) NOT NULL,
  `password` varchar(300) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `left_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `picture` (`picture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `teacher`:
--   `picture`
--       `uploaded_file` -> `name`
--

-- --------------------------------------------------------

--
-- Table structure for table `uploaded_file`
--

CREATE TABLE IF NOT EXISTS `uploaded_file` (
  `name` varchar(100) NOT NULL,
  `path` varchar(500) NOT NULL,
  `mime` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`name`),
  UNIQUE KEY `path` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `uploaded_file`:
--

-- --------------------------------------------------------

--
-- Structure for view `routine_student`
--
DROP TABLE IF EXISTS `routine_student`;

CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY INVOKER VIEW `routine_student`  AS  select `routine`.`id` AS `routine_id`,`class_routine`.`id` AS `id`,`subject`.`name` AS `subject`,`teacher`.`name` AS `teacher`,`batch`.`id` AS `batch` from (((((`class_routine` join `subject`) join `teacher` on(`class_routine`.`subject_id` = `subject`.`code` and `class_routine`.`teacher` = `teacher`.`id`)) left join `class` on(`class_routine`.`id` = `class`.`routine_id`)) join `routine` on(`class_routine`.`routine_id` = `routine`.`id`)) join `batch` on(`batch`.`id` = `routine`.`batch`)) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_ibfk_1` FOREIGN KEY (`batch`) REFERENCES `batch` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `assignments_ibfk_2` FOREIGN KEY (`routine`) REFERENCES `routine` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assignments_ibfk_3` FOREIGN KEY (`teacher`) REFERENCES `teacher` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `assignments_submissions`
--
ALTER TABLE `assignments_submissions`
  ADD CONSTRAINT `assignments_submissions_ibfk_1` FOREIGN KEY (`assignment`) REFERENCES `assignments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assignments_submissions_ibfk_2` FOREIGN KEY (`student`) REFERENCES `student` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `assignments_submissions_ibfk_3` FOREIGN KEY (`file`) REFERENCES `uploaded_file` (`name`) ON UPDATE CASCADE;

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attendance_ibfk_2` FOREIGN KEY (`teacher`) REFERENCES `teacher` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `attendance_record`
--
ALTER TABLE `attendance_record`
  ADD CONSTRAINT `attendance_record_ibfk_1` FOREIGN KEY (`student`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attendance_record_ibfk_2` FOREIGN KEY (`day`) REFERENCES `attendance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `batch`
--
ALTER TABLE `batch`
  ADD CONSTRAINT `batch_ibfk_1` FOREIGN KEY (`course`) REFERENCES `course` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_2` FOREIGN KEY (`taught_by`) REFERENCES `teacher` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `class_ibfk_4` FOREIGN KEY (`routine_id`) REFERENCES `class_routine` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `class_resources`
--
ALTER TABLE `class_resources`
  ADD CONSTRAINT `class_resources_ibfk_1` FOREIGN KEY (`for_class`) REFERENCES `class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `class_resources_ibfk_2` FOREIGN KEY (`resource`) REFERENCES `uploaded_file` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `class_routine`
--
ALTER TABLE `class_routine`
  ADD CONSTRAINT `class_routine_ibfk_2` FOREIGN KEY (`routine_id`) REFERENCES `routine` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `class_routine_ibfk_3` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`code`) ON UPDATE CASCADE,
  ADD CONSTRAINT `class_routine_ibfk_4` FOREIGN KEY (`teacher`) REFERENCES `teacher` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `notices`
--
ALTER TABLE `notices`
  ADD CONSTRAINT `notices_ibfk_1` FOREIGN KEY (`for_batch`) REFERENCES `batch` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `notices_ibfk_2` FOREIGN KEY (`issued_by`) REFERENCES `teacher` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `notices_ibfk_3` FOREIGN KEY (`file`) REFERENCES `uploaded_file` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `routine`
--
ALTER TABLE `routine`
  ADD CONSTRAINT `routine_ibfk_1` FOREIGN KEY (`batch`) REFERENCES `batch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`batch`) REFERENCES `batch` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `student_ibfk_2` FOREIGN KEY (`picture`) REFERENCES `uploaded_file` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `subject_ibfk_1` FOREIGN KEY (`part_of`) REFERENCES `course` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `subject_ibfk_2` FOREIGN KEY (`taught_by`) REFERENCES `teacher` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`picture`) REFERENCES `uploaded_file` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
