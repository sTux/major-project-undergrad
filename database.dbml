Enum week_day {
  Monday
  Tuesday
  Wednesday
  Thrusday
  Friday
  Saturday
  Sunday
}

Enum resource_type {
  lecture
  notes
  worksheet
}

Table uploaded_file {
  name varchar [pk]
  path varchar [unique]
  mime varchar
  created timestamp [default: `now()`]
}

Table course {
  id varchar [pk]
  name varchar [unique]
}

Table batch {
  id varchar [pk]
  course varchar [ref: > course.id]
}

Table subject {
  code varchar [pk]
  taught_by varchar [ref: > teacher.id]
  name varchar [unique]
  part_of varchar [ref: > course.id]
}

Table teacher {
  id varchar [pk]
  password varchar [default: "teacher"]
  name varchar
  picture varchar [null, ref: > uploaded_file.name]
}

Table student {
  id varchar [pk]
  password varchar [default: "student"]
  name varchar
  picture varchar [ref: > uploaded_file.name]
  batch varchar [ref: > batch.id]
}

Table notices {
  id bigint [pk, increment]
  title varchar
  text text [null]
  file varchar [null, ref: > uploaded_file.name]
  issued timestamp [default: `now()`]
  issued_by varchar [ref: > teacher.id]
  for_batch varchar [ref: > batch.id]
}

Table routine {
  id bigint [pk, increment]
  batch varchar [ref: > batch.id]
  day week_day
}

Table class_routine {
  id bigint [pk, increment]
  subject_id varchar [ref: > subject.code]
  routine_id bigint [ref: > routine.id]
  teacher varchar [ref: > teacher.id]
}

Table class {
  id bigint [pk, increment]
  topic varchar
  taught_by varchar [ref: > teacher.id]
  week smallint
  routine_id bigint [ref: > class_routine.id]
}

Table class_resources {
  id bigint [pk, increment]
  name varchar
  type resource_type
  resource varchar [unique, ref: > uploaded_file.name]
  for_class bigint [ref: > class.id]
}

Table attendance {
  id bigint [pk, increment]
  day date
  teacher varchar [ref: > teacher.id]
  class_id bigint [ref: > class.id]
}

Table attendance_record {
  day bigint [ref: > attendance.id]
  student varchar [ref: > student.id]
  joined timestamp [default: `now()`]
}

Table admin_passwords {
  created_at datetime [pk, default: `now()`]
  password varchar
  deleted_at datetime [null]
}
