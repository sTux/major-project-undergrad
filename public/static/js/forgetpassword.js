function passwordStrength(password) {
  /**
   * @author Sagnik @sagnik_eb
   * @description This function checks the password against a set of pre-defined regex to see if the password is valid or not
   * @description Once the password is found valid against it activates the confirm password box and checks for password strength against regex
   **/
  let strongRegex = new RegExp(
    "^(?=.{15,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$",
    "g"
  );
  let mediumRegex = new RegExp(
    "^(?=.{12,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$",
    "g"
  );
  let enoughRegex = new RegExp(
    "^(?=.{10,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$",
    "g"
  );

  if (!enoughRegex.test(password)) {
    return {
      html: `
                    <span style="color:red"> Password have <ul>
                    <li>One alphabet</li>
                    <li>One Number</li>
                    <li>One punctuation</li>
                    <li>10 characters</li>
                    </ul></span>
                 `,
      enable: false,
    };
  }
  if (strongRegex.test(password))
    return {
      html: '<span style="color:green"> Strong! </span>',
      enable: true,
    };
  else if (mediumRegex.test(password))
    return {
      html: '<span style="color:orange"> Medium! </span>',
      enable: true,
    };
  else
    return {
      html: '<span style="color:red"> Weak! </span>',
      enable: true,
    };
}

function passwordMatcher(password, cpassword) {
  /**
   * @description This function checks both the password field values on each keystroke to see if both the passwords are same
   **/
  return cpassword === password;
}
